use PASSIO_COFFEE;


-- Promotion
create table Promotion(
	`promotion_id` bigint primary key not null auto_increment,
    `name` nvarchar(100),
    `from_time` TIMESTAMP,
    `to_time` TIMESTAMP
);
create table Promotion_Item(
	`promotion_id` bigint not null auto_increment,
    `item_id` bigint,
	CONSTRAINT PK PRIMARY KEY (promotion_id,item_id)
);


-- Notification
create table Notification(
	`notification_id` bigint primary key not null auto_increment,
    `content` nvarchar(100),
    `image` LONGBLOB,
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,	
    `status` varchar(15)
);
create table User_Notification(
	`user_notification_id` bigint not null auto_increment,
	`notification_id` bigint,
	CONSTRAINT PK PRIMARY KEY (user_notification_id,notification_id)
);


-- Card && Coupon
create table Coupon(
	`coupon_id` bigint primary key not null auto_increment,
    `from_time` TIMESTAMP,
    `to_time` TIMESTAMP,
    `type_of_coupon` nvarchar(45),
    `type_of_card` nvarchar(45),
    `price_condition` int,
    `discount` int,
    `status` varchar(15)
);

create table Card_Coupons (
	`user_card_id` bigint not null auto_increment,
    `coupon_id` bigint,
	CONSTRAINT PK PRIMARY KEY (user_card_id,coupon_id)
);

create table User_Card(
	`user_card_id` bigint primary key auto_increment,
    `type` nvarchar(45),
    `reward` varchar(15),
    `card_coupons_id` bigint
);

-- Review && Respone
create table Respone(
	`respone_id` bigint primary key not null auto_increment,
    `shop_id` bigint,
    `item_id` bigint,
    `content` nvarchar(100),
    `image` LONGBLOB,
    `time` TIMESTAMP
);
create table Review(
	`review_id` bigint primary key not null auto_increment,
    `user_id` bigint,
    `content` nvarchar(100),
    `image` LONGBLOB,
    `rating` int,
    `time` TIMESTAMP,
    `respone_id` bigint
);

-- Shop
create table Shop_Reviews(
	`shop_reviews_id` bigint not null ,
    `review_id` bigint,
	CONSTRAINT PK PRIMARY KEY (review_id,shop_reviews_id)
);
create table Shop(
	`shop_id` bigint primary key not null auto_increment,
    `name` nvarchar(100),
    `image` LONGBLOB,
    `address` nvarchar(100),
    `phone` varchar(15),
    `likes` varchar(15),
    `avg_rating` int,
	`shop_reviews_id` bigint,
	`shop_list_item` bigint
);



-- Category && Item
create table Category(
	`category_id` bigint primary key not null auto_increment,
    `category_item_name` nvarchar(100),
    `image` LONGBLOB
);
create table Category_Item(
	`category_id` bigint not null auto_increment,
    `item_id` bigint,
	CONSTRAINT PK PRIMARY KEY (category_id,item_id)
);
create table Item_Reviews(
	`item_reviews_id` bigint not null,
    `review_id` bigint,
	CONSTRAINT PK PRIMARY KEY (review_id,item_reviews_id)
);
create table Item(
	`item_id` bigint primary key not null auto_increment,
    `shop_id` bigint,
    `category_item_id` bigint,
    `promotion_id` bigint,
    `name` nvarchar(100),
    `image` LONGBLOB,
    `price` int,
    `description` nvarchar(100),
    `likes` varchar(15),
    `avg_rating` int,
	`item_reviews_id` bigint
);



-- User Cart && User Favorite && User Order
create table User_Cart(
	`user_cart_id` bigint not null auto_increment,
	`item_id` bigint,
	CONSTRAINT PK PRIMARY KEY (user_cart_id,item_id)
);

create table User_Favorite(
	`user_favorite_id` bigint not null auto_increment,
	`item_id` bigint,
	CONSTRAINT PK PRIMARY KEY (user_favorite_id,item_id)
);

create table Ordered_Item_List(
	`ordered_item_list_id` bigint not null auto_increment,
	`item_id` bigint,
	CONSTRAINT PK PRIMARY KEY (ordered_item_list_id,item_id)
);

create table User_Order(
	`user_order_id` bigint not null,
	`shop_id` bigint,
    `ordered_item_list_id` bigint,
    `from_time` TIMESTAMP,
    `to_time` TIMESTAMP,
    `status` varchar(15),
    CONSTRAINT PK PRIMARY KEY (ordered_item_list_id,user_order_id)
);

-- User
create table User(
	`user_id` bigint primary key not null auto_increment,
    `name` nvarchar(45),
    `avatar` LONGBLOB,
    `phone` varchar(15),
    `address` nvarchar(100),
	`user_order_id` bigint,
	`user_card_id` bigint,
	`user_favorite_id` bigint,
	`user_notification_id` bigint,
	`user_cart_id` bigint
);

ALTER TABLE Promotion_Item
ADD CONSTRAINT FK_Deal FOREIGN KEY (promotion_id) REFERENCES Promotion(promotion_id) ON DELETE CASCADE;
ALTER TABLE Promotion_Item
ADD CONSTRAINT FK_Promotion_Item FOREIGN KEY (item_id) REFERENCES Item(item_id);
ALTER TABLE User_Notification
ADD CONSTRAINT FK_Notifications FOREIGN KEY (notification_id) REFERENCES Notification(notification_id);
ALTER TABLE User_Notification
ADD CONSTRAINT FK_User_Notification FOREIGN KEY (user_notification_id) REFERENCES User(user_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE Card_Coupons
ADD CONSTRAINT FK_Coupon FOREIGN KEY (coupon_id) REFERENCES Coupon(coupon_id);
ALTER TABLE Card_Coupons
ADD CONSTRAINT FK_Card_Coupons FOREIGN KEY (user_card_id) REFERENCES User_Card(user_card_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE User_Card
ADD CONSTRAINT FK_User_Card FOREIGN KEY (user_card_id) REFERENCES User(user_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE Respone
ADD CONSTRAINT FK_Respone FOREIGN KEY (respone_id) REFERENCES Review(review_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE Shop_Reviews
ADD CONSTRAINT FK_Shop_Reviews FOREIGN KEY (shop_reviews_id) REFERENCES Shop(shop_id) ON DELETE CASCADE;
ALTER TABLE Item_Reviews
ADD CONSTRAINT FK_Item_Reviews FOREIGN KEY (item_reviews_id) REFERENCES Item(item_id)  ON DELETE CASCADE;
ALTER TABLE Review
ADD CONSTRAINT FK_Review_Of_Shop FOREIGN KEY (review_id) REFERENCES Shop_Reviews(review_id) ON DELETE CASCADE ;
ALTER TABLE Review
ADD CONSTRAINT FK_Review_Of_Item FOREIGN KEY (review_id) REFERENCES Item_Reviews(review_id) ON DELETE CASCADE;
ALTER TABLE Category_Item
ADD CONSTRAINT FK_Category FOREIGN KEY (category_id) REFERENCES Category(category_id);
ALTER TABLE Category_Item
ADD CONSTRAINT FK_Category_Item FOREIGN KEY (item_id) REFERENCES Item(item_id) ON DELETE CASCADE;
ALTER TABLE User_Cart
ADD CONSTRAINT FK_Cart FOREIGN KEY (item_id) REFERENCES Item(item_id);
ALTER TABLE User_Cart
ADD CONSTRAINT FK_User_Cart FOREIGN KEY (user_cart_id) REFERENCES User(user_id) ON DELETE CASCADE;
ALTER TABLE User_Favorite
ADD CONSTRAINT FK_Favorite FOREIGN KEY (item_id) REFERENCES Item(item_id);
ALTER TABLE User_Favorite
ADD CONSTRAINT FK_User_Favorite FOREIGN KEY (user_favorite_id) REFERENCES User(user_id) ON DELETE CASCADE;
ALTER TABLE Ordered_Item_List
ADD CONSTRAINT FK_Order FOREIGN KEY (ordered_item_list_id) REFERENCES User_Order(ordered_item_list_id) ON DELETE CASCADE;
ALTER TABLE Ordered_Item_List
ADD CONSTRAINT FK_Order_Item FOREIGN KEY (item_id) REFERENCES Item(item_id) ON DELETE CASCADE;
ALTER TABLE User_Order
ADD CONSTRAINT FK_Order_Shop FOREIGN KEY (shop_id) REFERENCES Shop(shop_id) ON DELETE CASCADE;
ALTER TABLE User_Order
ADD CONSTRAINT FK_User_Order FOREIGN KEY (user_order_id) REFERENCES User(user_id) ON DELETE CASCADE;
ALTER TABLE Item
ADD CONSTRAINT FK_Item FOREIGN KEY (shop_id) REFERENCES Shop(shop_id) ON DELETE CASCADE;
ALTER TABLE Review
ADD CONSTRAINT FK_Review_User FOREIGN KEY (user_id) REFERENCES User(user_id) ON DELETE CASCADE;
ALTER TABLE Respone
ADD CONSTRAINT FK_Respone_Shop FOREIGN KEY (shop_id) REFERENCES Shop(shop_id) ON DELETE CASCADE;
ALTER TABLE Respone
ADD CONSTRAINT FK_Respone_Item FOREIGN KEY (item_id) REFERENCES Item(item_id) ON DELETE CASCADE;

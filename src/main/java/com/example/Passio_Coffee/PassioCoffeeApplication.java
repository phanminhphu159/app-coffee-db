package com.example.Passio_Coffee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PassioCoffeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PassioCoffeeApplication.class, args);
	}

}

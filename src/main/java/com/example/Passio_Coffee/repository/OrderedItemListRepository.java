package com.example.Passio_Coffee.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.Passio_Coffee.model.db.Entity.OrderedItemListEntity;

@Repository
public interface OrderedItemListRepository extends JpaRepository<OrderedItemListEntity, Long> {

}

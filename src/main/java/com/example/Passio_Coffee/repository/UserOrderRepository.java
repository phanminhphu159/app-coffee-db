package com.example.Passio_Coffee.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.Passio_Coffee.model.db.Entity.UserOrderEntity;

@Repository
public interface UserOrderRepository extends JpaRepository<UserOrderEntity, Long> {

}

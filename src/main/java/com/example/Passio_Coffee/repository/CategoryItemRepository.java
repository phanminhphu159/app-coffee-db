package com.example.Passio_Coffee.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.Passio_Coffee.model.db.Entity.CategoryItemEntity;

@Repository
public interface CategoryItemRepository extends JpaRepository<CategoryItemEntity, Long> {

}

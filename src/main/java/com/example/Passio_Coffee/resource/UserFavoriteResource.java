package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.UserFavorite;
import com.example.Passio_Coffee.model.client.Request.UserFavoriteRequest;
import com.example.Passio_Coffee.service.UserFavoriteService;

@Path("/UserFavorite")
@Component
public class UserFavoriteResource {

    @Autowired
    UserFavoriteService UserFavoriteService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<UserFavorite> getAll() {
        return UserFavoriteService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserFavorite save(final UserFavoriteRequest UserFavoriteRequest) {
        return UserFavoriteService.save(UserFavoriteRequest);
    }
    
    @PUT
    @Path("{UserFavorite_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserFavorite update(@PathParam("UserFavorite_id") final Long id, final UserFavoriteRequest UserFavoriteRequest) {
        return UserFavoriteService.update(id, UserFavoriteRequest);
    }
    
    @DELETE
    @Path("{UserFavorite_id}")
    public void delete(@PathParam("UserFavorite_id") final Long id) {
        UserFavoriteService.delete(id);
    }

}
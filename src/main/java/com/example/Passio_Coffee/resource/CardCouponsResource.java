package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.CardCoupons;
import com.example.Passio_Coffee.model.client.Request.CardCouponsRequest;
import com.example.Passio_Coffee.service.CardCouponsService;

@Path("/CardCoupons")
@Component
public class CardCouponsResource {

    @Autowired
    CardCouponsService CardCouponsService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<CardCoupons> getAll() {
        return CardCouponsService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CardCoupons save(final CardCouponsRequest CardCouponsRequest) {
        return CardCouponsService.save(CardCouponsRequest);
    }
    
    @PUT
    @Path("{CardCoupons_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CardCoupons update(@PathParam("CardCoupons_id") final Long id, final CardCouponsRequest CardCouponsRequest) {
        return CardCouponsService.update(id, CardCouponsRequest);
    }
    
    @DELETE
    @Path("{CardCoupons_id}")
    public void delete(@PathParam("CardCoupons_id") final Long id) {
        CardCouponsService.delete(id);
    }

}
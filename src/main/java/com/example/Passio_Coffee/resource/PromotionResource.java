package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.Promotion;
import com.example.Passio_Coffee.model.client.Request.PromotionRequest;
import com.example.Passio_Coffee.service.PromotionService;

@Path("/Promotion")
@Component
public class PromotionResource {

    @Autowired
    PromotionService PromotionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Promotion> getAll() {
        return PromotionService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Promotion save(final PromotionRequest PromotionRequest) {
        return PromotionService.save(PromotionRequest);
    }
    
    @PUT
    @Path("{Promotion_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Promotion update(@PathParam("Promotion_id") final Long id, final PromotionRequest PromotionRequest) {
        return PromotionService.update(id, PromotionRequest);
    }
    
    @DELETE
    @Path("{Promotion_id}")
    public void delete(@PathParam("Promotion_id") final Long id) {
        PromotionService.delete(id);
    }

}
package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.Shop;
import com.example.Passio_Coffee.model.client.Request.ShopRequest;
import com.example.Passio_Coffee.service.ShopService;

@Path("/Shop")
@Component
public class ShopResource {

    @Autowired
    ShopService ShopService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Shop> getAll() {
        return ShopService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Shop save(final ShopRequest ShopRequest) {
        return ShopService.save(ShopRequest);
    }
    
    @PUT
    @Path("{Shop_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Shop update(@PathParam("Shop_id") final Long id, final ShopRequest ShopRequest) {
        return ShopService.update(id, ShopRequest);
    }
    
    @DELETE
    @Path("{Shop_id}")
    public void delete(@PathParam("Shop_id") final Long id) {
        ShopService.delete(id);
    }

}
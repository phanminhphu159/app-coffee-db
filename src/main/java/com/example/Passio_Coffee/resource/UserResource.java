package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.User;
import com.example.Passio_Coffee.model.client.Request.UserRequest;
import com.example.Passio_Coffee.service.UserService;

@Path("/User")
@Component
public class UserResource {

    @Autowired
    UserService userService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<User> getAll() {
        return userService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User save(final UserRequest userRequest) {
        return userService.save(userRequest);
    }
    
    @PUT
    @Path("{user_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User update(@PathParam("user_id") final Long id, final UserRequest userRequest) {
        return userService.update(id, userRequest);
    }
    
    @DELETE
    @Path("{user_id}")
    public void delete(@PathParam("user_id") final Long id) {
        userService.delete(id);
    }

}
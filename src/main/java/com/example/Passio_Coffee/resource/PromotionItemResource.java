package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.PromotionItem;
import com.example.Passio_Coffee.model.client.Request.PromotionItemRequest;
import com.example.Passio_Coffee.service.PromotionItemService;

@Path("/PromotionItem")
@Component
public class PromotionItemResource {

    @Autowired
    PromotionItemService PromotionItemService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<PromotionItem> getAll() {
        return PromotionItemService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public PromotionItem save(final PromotionItemRequest PromotionItemRequest) {
        return PromotionItemService.save(PromotionItemRequest);
    }
    
    @PUT
    @Path("{PromotionItem_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public PromotionItem update(@PathParam("PromotionItem_id") final Long id, final PromotionItemRequest PromotionItemRequest) {
        return PromotionItemService.update(id, PromotionItemRequest);
    }
    
    @DELETE
    @Path("{PromotionItem_id}")
    public void delete(@PathParam("PromotionItem_id") final Long id) {
        PromotionItemService.delete(id);
    }

}
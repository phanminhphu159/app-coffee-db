package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.Review;
import com.example.Passio_Coffee.model.client.Request.ReviewRequest;
import com.example.Passio_Coffee.service.ReviewService;

@Path("/Review")
@Component
public class ReviewResource {

    @Autowired
    ReviewService ReviewService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Review> getAll() {
        return ReviewService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Review save(final ReviewRequest ReviewRequest) {
        return ReviewService.save(ReviewRequest);
    }
    
    @PUT
    @Path("{Review_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Review update(@PathParam("Review_id") final Long id, final ReviewRequest ReviewRequest) {
        return ReviewService.update(id, ReviewRequest);
    }
    
    @DELETE
    @Path("{Review_id}")
    public void delete(@PathParam("Review_id") final Long id) {
        ReviewService.delete(id);
    }

}
package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.Notification;
import com.example.Passio_Coffee.model.client.Request.NotificationRequest;
import com.example.Passio_Coffee.service.NotificationService;

@Path("/Notification")
@Component
public class NotificationResource {

    @Autowired
    NotificationService NotificationService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Notification> getAll() {
        return NotificationService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Notification save(final NotificationRequest NotificationRequest) {
        return NotificationService.save(NotificationRequest);
    }
    
    @PUT
    @Path("{Notification_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Notification update(@PathParam("Notification_id") final Long id, final NotificationRequest NotificationRequest) {
        return NotificationService.update(id, NotificationRequest);
    }
    
    @DELETE
    @Path("{Notification_id}")
    public void delete(@PathParam("Notification_id") final Long id) {
        NotificationService.delete(id);
    }

}
package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.Category;
import com.example.Passio_Coffee.model.client.Request.CategoryRequest;
import com.example.Passio_Coffee.service.CategoryService;

@Path("/Category")
@Component
public class CategoryResource {

    @Autowired
    CategoryService CategoryService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Category> getAll() {
        return CategoryService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Category save(final CategoryRequest CategoryRequest) {
        return CategoryService.save(CategoryRequest);
    }
    
    @PUT
    @Path("{Category_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Category update(@PathParam("Category_id") final Long id, final CategoryRequest CategoryRequest) {
        return CategoryService.update(id, CategoryRequest);
    }
    
    @DELETE
    @Path("{Category_id}")
    public void delete(@PathParam("Category_id") final Long id) {
        CategoryService.delete(id);
    }

}
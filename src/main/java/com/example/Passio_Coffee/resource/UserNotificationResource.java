package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.UserNotification;
import com.example.Passio_Coffee.model.client.Request.UserNotificationRequest;
import com.example.Passio_Coffee.service.UserNotificationService;

@Path("/UserNotification")
@Component
public class UserNotificationResource {

    @Autowired
    UserNotificationService UserNotificationService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<UserNotification> getAll() {
        return UserNotificationService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserNotification save(final UserNotificationRequest UserNotificationRequest) {
        return UserNotificationService.save(UserNotificationRequest);
    }
    
    @PUT
    @Path("{UserNotification_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserNotification update(@PathParam("UserNotification_id") final Long id, final UserNotificationRequest UserNotificationRequest) {
        return UserNotificationService.update(id, UserNotificationRequest);
    }
    
    @DELETE
    @Path("{UserNotification_id}")
    public void delete(@PathParam("UserNotification_id") final Long id) {
        UserNotificationService.delete(id);
    }

}
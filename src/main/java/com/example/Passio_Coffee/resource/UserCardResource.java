package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.UserCard;
import com.example.Passio_Coffee.model.client.Request.UserCardRequest;
import com.example.Passio_Coffee.service.UserCardService;

@Path("/UserCard")
@Component
public class UserCardResource {

    @Autowired
    UserCardService UserCardService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<UserCard> getAll() {
        return UserCardService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserCard save(final UserCardRequest UserCardRequest) {
        return UserCardService.save(UserCardRequest);
    }
    
    @PUT
    @Path("{UserCard_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserCard update(@PathParam("UserCard_id") final Long id, final UserCardRequest UserCardRequest) {
        return UserCardService.update(id, UserCardRequest);
    }
    
    @DELETE
    @Path("{UserCard_id}")
    public void delete(@PathParam("UserCard_id") final Long id) {
        UserCardService.delete(id);
    }

}
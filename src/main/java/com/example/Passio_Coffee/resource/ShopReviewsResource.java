package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.ShopReviews;
import com.example.Passio_Coffee.model.client.Request.ShopReviewsRequest;
import com.example.Passio_Coffee.service.ShopReviewsService;

@Path("/ShopReviews")
@Component
public class ShopReviewsResource {

    @Autowired
    ShopReviewsService ShopReviewsService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<ShopReviews> getAll() {
        return ShopReviewsService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ShopReviews save(final ShopReviewsRequest ShopReviewsRequest) {
        return ShopReviewsService.save(ShopReviewsRequest);
    }
    
    @PUT
    @Path("{ShopReviews_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ShopReviews update(@PathParam("ShopReviews_id") final Long id, final ShopReviewsRequest ShopReviewsRequest) {
        return ShopReviewsService.update(id, ShopReviewsRequest);
    }
    
    @DELETE
    @Path("{ShopReviews_id}")
    public void delete(@PathParam("ShopReviews_id") final Long id) {
        ShopReviewsService.delete(id);
    }

}
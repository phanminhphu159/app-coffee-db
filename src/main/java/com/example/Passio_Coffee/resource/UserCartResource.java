package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.UserCart;
import com.example.Passio_Coffee.model.client.Request.UserCartRequest;
import com.example.Passio_Coffee.service.UserCartService;

@Path("/UserCart")
@Component
public class UserCartResource {

    @Autowired
    UserCartService UserCartService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<UserCart> getAll() {
        return UserCartService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserCart save(final UserCartRequest UserCartRequest) {
        return UserCartService.save(UserCartRequest);
    }
    
    @PUT
    @Path("{UserCart_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserCart update(@PathParam("UserCart_id") final Long id, final UserCartRequest UserCartRequest) {
        return UserCartService.update(id, UserCartRequest);
    }
    
    @DELETE
    @Path("{UserCart_id}")
    public void delete(@PathParam("UserCart_id") final Long id) {
        UserCartService.delete(id);
    }

}
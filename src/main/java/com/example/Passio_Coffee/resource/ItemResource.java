package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.Item;
import com.example.Passio_Coffee.model.client.Request.ItemRequest;
import com.example.Passio_Coffee.service.ItemService;

@Path("/Item")
@Component
public class ItemResource {

    @Autowired
    ItemService ItemService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Item> getAll() {
        return ItemService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Item save(final ItemRequest ItemRequest) {
        return ItemService.save(ItemRequest);
    }
    
    @PUT
    @Path("{Item_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Item update(@PathParam("Item_id") final Long id, final ItemRequest ItemRequest) {
        return ItemService.update(id, ItemRequest);
    }
    
    @DELETE
    @Path("{Item_id}")
    public void delete(@PathParam("Item_id") final Long id) {
        ItemService.delete(id);
    }

}
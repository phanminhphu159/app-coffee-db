package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.CategoryItem;
import com.example.Passio_Coffee.model.client.Request.CategoryItemRequest;
import com.example.Passio_Coffee.service.CategoryItemService;

@Path("/CategoryItem")
@Component
public class CategoryItemResource {

    @Autowired
    CategoryItemService CategoryItemService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<CategoryItem> getAll() {
        return CategoryItemService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CategoryItem save(final CategoryItemRequest CategoryItemRequest) {
        return CategoryItemService.save(CategoryItemRequest);
    }
    
    @PUT
    @Path("{CategoryItem_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CategoryItem update(@PathParam("CategoryItem_id") final Long id, final CategoryItemRequest CategoryItemRequest) {
        return CategoryItemService.update(id, CategoryItemRequest);
    }
    
    @DELETE
    @Path("{CategoryItem_id}")
    public void delete(@PathParam("CategoryItem_id") final Long id) {
        CategoryItemService.delete(id);
    }

}
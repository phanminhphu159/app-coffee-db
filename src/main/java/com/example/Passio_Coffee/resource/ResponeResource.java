package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.Respone;
import com.example.Passio_Coffee.model.client.Request.ResponeRequest;
import com.example.Passio_Coffee.service.ResponeService;

@Path("/Respone")
@Component
public class ResponeResource {

    @Autowired
    ResponeService ResponeService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Respone> getAll() {
        return ResponeService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Respone save(final ResponeRequest ResponeRequest) {
        return ResponeService.save(ResponeRequest);
    }
    
    @PUT
    @Path("{Respone_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Respone update(@PathParam("Respone_id") final Long id, final ResponeRequest ResponeRequest) {
        return ResponeService.update(id, ResponeRequest);
    }
    
    @DELETE
    @Path("{Respone_id}")
    public void delete(@PathParam("Respone_id") final Long id) {
        ResponeService.delete(id);
    }

}
package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.Coupon;
import com.example.Passio_Coffee.model.client.Request.CouponRequest;
import com.example.Passio_Coffee.service.CouponService;

@Path("/Coupon")
@Component
public class CouponResource {

    @Autowired
    CouponService CouponService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Coupon> getAll() {
        return CouponService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Coupon save(final CouponRequest CouponRequest) {
        return CouponService.save(CouponRequest);
    }
    
    @PUT
    @Path("{Coupon_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Coupon update(@PathParam("Coupon_id") final Long id, final CouponRequest CouponRequest) {
        return CouponService.update(id, CouponRequest);
    }
    
    @DELETE
    @Path("{Coupon_id}")
    public void delete(@PathParam("Coupon_id") final Long id) {
        CouponService.delete(id);
    }

}
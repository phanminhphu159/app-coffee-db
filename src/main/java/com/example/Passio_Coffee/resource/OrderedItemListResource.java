package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.OrderedItemList;
import com.example.Passio_Coffee.model.client.Request.OrderedItemListRequest;
import com.example.Passio_Coffee.service.OrderedItemListService;

@Path("/OrderedItemList")
@Component
public class OrderedItemListResource {

    @Autowired
    OrderedItemListService OrderedItemListService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<OrderedItemList> getAll() {
        return OrderedItemListService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public OrderedItemList save(final OrderedItemListRequest OrderedItemListRequest) {
        return OrderedItemListService.save(OrderedItemListRequest);
    }
    
    @PUT
    @Path("{OrderedItemList_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public OrderedItemList update(@PathParam("OrderedItemList_id") final Long id, final OrderedItemListRequest OrderedItemListRequest) {
        return OrderedItemListService.update(id, OrderedItemListRequest);
    }
    
    @DELETE
    @Path("{OrderedItemList_id}")
    public void delete(@PathParam("OrderedItemList_id") final Long id) {
        OrderedItemListService.delete(id);
    }

}
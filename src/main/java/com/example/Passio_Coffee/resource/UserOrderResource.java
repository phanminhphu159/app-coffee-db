package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.UserOrder;
import com.example.Passio_Coffee.model.client.Request.UserOrderRequest;
import com.example.Passio_Coffee.service.UserOrderService;

@Path("/UserOrder")
@Component
public class UserOrderResource {

    @Autowired
    UserOrderService UserOrderService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<UserOrder> getAll() {
        return UserOrderService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserOrder save(final UserOrderRequest UserOrderRequest) {
        return UserOrderService.save(UserOrderRequest);
    }
    
    @PUT
    @Path("{UserOrder_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserOrder update(@PathParam("UserOrder_id") final Long id, final UserOrderRequest UserOrderRequest) {
        return UserOrderService.update(id, UserOrderRequest);
    }
    
    @DELETE
    @Path("{UserOrder_id}")
    public void delete(@PathParam("UserOrder_id") final Long id) {
        UserOrderService.delete(id);
    }

}
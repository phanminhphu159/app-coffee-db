package com.example.Passio_Coffee.resource;

import java.util.Collection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.model.client.Respone.ItemReviews;
import com.example.Passio_Coffee.model.client.Request.ItemReviewsRequest;
import com.example.Passio_Coffee.service.ItemReviewsService;

@Path("/ItemReviews")
@Component
public class ItemReviewsResource {

    @Autowired
    ItemReviewsService ItemReviewsService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<ItemReviews> getAll() {
        return ItemReviewsService.getAll();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ItemReviews save(final ItemReviewsRequest ItemReviewsRequest) {
        return ItemReviewsService.save(ItemReviewsRequest);
    }
    
    @PUT
    @Path("{ItemReviews_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ItemReviews update(@PathParam("ItemReviews_id") final Long id, final ItemReviewsRequest ItemReviewsRequest) {
        return ItemReviewsService.update(id, ItemReviewsRequest);
    }
    
    @DELETE
    @Path("{ItemReviews_id}")
    public void delete(@PathParam("ItemReviews_id") final Long id) {
        ItemReviewsService.delete(id);
    }

}
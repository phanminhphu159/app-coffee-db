package com.example.Passio_Coffee;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.resource.CardCouponsResource;
import com.example.Passio_Coffee.resource.CategoryItemResource;
import com.example.Passio_Coffee.resource.CategoryResource;
import com.example.Passio_Coffee.resource.CouponResource;
import com.example.Passio_Coffee.resource.ItemResource;
import com.example.Passio_Coffee.resource.ItemReviewsResource;
import com.example.Passio_Coffee.resource.NotificationResource;
import com.example.Passio_Coffee.resource.OrderedItemListResource;
import com.example.Passio_Coffee.resource.PromotionItemResource;
import com.example.Passio_Coffee.resource.PromotionResource;
import com.example.Passio_Coffee.resource.ResponeResource;
import com.example.Passio_Coffee.resource.ReviewResource;
import com.example.Passio_Coffee.resource.ShopResource;
import com.example.Passio_Coffee.resource.ShopReviewsResource;
import com.example.Passio_Coffee.resource.UserCardResource;
import com.example.Passio_Coffee.resource.UserCartResource;
import com.example.Passio_Coffee.resource.UserFavoriteResource;
import com.example.Passio_Coffee.resource.UserNotificationResource;
import com.example.Passio_Coffee.resource.UserOrderResource;
import com.example.Passio_Coffee.resource.UserResource;

@Component
public class JerseyConfiguration extends ResourceConfig {

    public JerseyConfiguration() {
        register(UserResource.class);
        register(UserOrderResource.class);
        register(UserNotificationResource.class);
        register(UserFavoriteResource.class);
        register(UserCartResource.class);
        register(UserCardResource.class);
        register(ShopReviewsResource.class);
        register(ShopResource.class);
        register(ReviewResource.class);
        register(ResponeResource.class);
        register(PromotionItemResource.class);
        register(PromotionResource.class);
        register(OrderedItemListResource.class);
        register(NotificationResource.class);
        register(ItemReviewsResource.class);
        register(ItemResource.class);
        register(CouponResource.class);
        register(CategoryItemResource.class);
        register(CategoryResource.class);
        register(CardCouponsResource.class);
    }

}
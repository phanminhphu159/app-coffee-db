package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.CardCoupons;
import com.example.Passio_Coffee.model.client.Request.CardCouponsRequest;
import com.example.Passio_Coffee.model.db.Entity.CardCouponsEntity;

@Component
public class CardCouponsConverter implements ModelConverter<CardCouponsRequest, CardCouponsEntity, CardCoupons> {

    @Override
    public CardCouponsEntity requestToModel(CardCouponsRequest request) {
        CardCouponsEntity model = new CardCouponsEntity();
        model.setUser_card_id(request.getUser_card_id());
        model.setCoupon_id(request.getCoupon_id());
        return model;
    }

    @Override
    public CardCoupons modelToResponse(CardCouponsEntity model) {
        CardCoupons response = new CardCoupons();
        response.setUser_card_id(model.getUser_card_id());
        response.setCoupon_id(model.getCoupon_id());
        return response;
    }

}

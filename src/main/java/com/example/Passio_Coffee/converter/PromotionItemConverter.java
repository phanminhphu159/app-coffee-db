package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.PromotionItem;
import com.example.Passio_Coffee.model.client.Request.PromotionItemRequest;
import com.example.Passio_Coffee.model.db.Entity.PromotionItemEntity;

@Component
public class PromotionItemConverter implements ModelConverter<PromotionItemRequest, PromotionItemEntity, PromotionItem> {

    @Override
    public PromotionItemEntity requestToModel(PromotionItemRequest request) {
        PromotionItemEntity model = new PromotionItemEntity();
        model.setPromotion_id(request.getPromotion_id());
        model.setItem_id(request.getItem_id());
        return model;
    }

    @Override
    public PromotionItem modelToResponse(PromotionItemEntity model) {
        PromotionItem response = new PromotionItem();
        response.setPromotion_id(model.getPromotion_id());
        response.setItem_id(model.getItem_id());
        return response;
    }

}

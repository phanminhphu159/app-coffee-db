package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.Category;
import com.example.Passio_Coffee.model.client.Request.CategoryRequest;
import com.example.Passio_Coffee.model.db.Entity.CategoryEntity;

@Component
public class CategoryConverter implements ModelConverter<CategoryRequest, CategoryEntity, Category> {

    @Override
    public CategoryEntity requestToModel(CategoryRequest request) {
        CategoryEntity model = new CategoryEntity();
        model.setCategory_id(request.getCategory_id());
        model.setCategory_item_name(request.getCategory_item_name());
        model.setImage(request.getImage());
        return model;
    }

    @Override
    public Category modelToResponse(CategoryEntity model) {
        Category response = new Category();
        response.setCategory_id(model.getCategory_id());
        response.setCategory_item_name(model.getCategory_item_name());
        response.setImage(model.getImage());
        return response;
    }

}

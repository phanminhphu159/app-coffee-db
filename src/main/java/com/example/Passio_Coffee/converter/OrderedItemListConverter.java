package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.OrderedItemList;
import com.example.Passio_Coffee.model.client.Request.OrderedItemListRequest;
import com.example.Passio_Coffee.model.db.Entity.OrderedItemListEntity;

@Component
public class OrderedItemListConverter implements ModelConverter<OrderedItemListRequest, OrderedItemListEntity, OrderedItemList> {

    @Override
    public OrderedItemListEntity requestToModel(OrderedItemListRequest request) {
        OrderedItemListEntity model = new OrderedItemListEntity();
        model.setOrdered_item_list_id(request.getOrdered_item_list_id());
        model.setItem_id(request.getItem_id());
        return model;
    }

    @Override
    public OrderedItemList modelToResponse(OrderedItemListEntity model) {
        OrderedItemList response = new OrderedItemList();
        response.setOrdered_item_list_id(model.getOrdered_item_list_id());
        response.setItem_id(model.getItem_id());
        return response;
    }

}

package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.ItemReviews;
import com.example.Passio_Coffee.model.client.Request.ItemReviewsRequest;
import com.example.Passio_Coffee.model.db.Entity.ItemReviewsEntity;

@Component
public class ItemReviewsConverter implements ModelConverter<ItemReviewsRequest, ItemReviewsEntity, ItemReviews> {

    @Override
    public ItemReviewsEntity requestToModel(ItemReviewsRequest request) {
        ItemReviewsEntity model = new ItemReviewsEntity();
        model.setItem_reviews_id(request.getItem_reviews_id());
        model.setReview_id(request.getReview_id());
        return model;
    }

    @Override
    public ItemReviews modelToResponse(ItemReviewsEntity model) {
        ItemReviews response = new ItemReviews();
        response.setItem_reviews_id(model.getItem_reviews_id());
        response.setReview_id(model.getReview_id());
        return response;
    }

}

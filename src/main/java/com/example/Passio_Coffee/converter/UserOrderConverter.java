package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Request.UserOrderRequest;
import com.example.Passio_Coffee.model.client.Respone.UserOrder;
import com.example.Passio_Coffee.model.db.Entity.UserOrderEntity;

@Component
public class UserOrderConverter implements ModelConverter<UserOrderRequest, UserOrderEntity, UserOrder> {

    @Override
    public UserOrderEntity requestToModel(UserOrderRequest request) {
        UserOrderEntity model = new UserOrderEntity();
        model.setUser_order_id(request.getUser_order_id());
        model.setShop_id(request.getShop_id());
        model.setOrdered_item_list_id(request.getOrdered_item_list_id());
        model.setFrom_time(request.getFrom_time());
        model.setTo_time(request.getTo_time());
        model.setStatus(request.getStatus());
        return model;
    }

    @Override
    public UserOrder modelToResponse(UserOrderEntity model) {
        UserOrder response = new UserOrder();
        response.setUser_order_id(model.getUser_order_id());
        response.setShop_id(model.getShop_id());
        response.setOrdered_item_list_id(model.getOrdered_item_list_id());
        response.setFrom_time(model.getFrom_time());
        response.setTo_time(model.getTo_time());
        response.setStatus(model.getStatus());
        return response;
    }

}


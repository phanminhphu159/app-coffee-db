package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.Promotion;
import com.example.Passio_Coffee.model.client.Request.PromotionRequest;
import com.example.Passio_Coffee.model.db.Entity.PromotionEntity;

@Component
public class PromotionConverter implements ModelConverter<PromotionRequest, PromotionEntity, Promotion> {

    @Override
    public PromotionEntity requestToModel(PromotionRequest request) {
        PromotionEntity model = new PromotionEntity();
        model.setPromotion_id(request.getPromotion_id());
        model.setName(request.getName());
        model.setFrom_time(request.getFrom_time());
        model.setTo_time(request.getTo_time());
        return model;
    }

    @Override
    public Promotion modelToResponse(PromotionEntity model) {
        Promotion response = new Promotion();
        response.setPromotion_id(model.getPromotion_id());
        response.setName(model.getName());
        response.setFrom_time(model.getFrom_time());
        response.setTo_time(model.getTo_time());
        return response;
    }

}

package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.UserCart;
import com.example.Passio_Coffee.model.client.Request.UserCartRequest;
import com.example.Passio_Coffee.model.db.Entity.UserCartEntity;

@Component
public class UserCartConverter implements ModelConverter<UserCartRequest, UserCartEntity, UserCart> {

    @Override
    public UserCartEntity requestToModel(UserCartRequest request) {
        UserCartEntity model = new UserCartEntity();
        model.setUser_cart_id(request.getUser_cart_id());
        model.setItem_id(request.getItem_id());
        return model;
    }

    @Override
    public UserCart modelToResponse(UserCartEntity model) {
        UserCart response = new UserCart();
        response.setUser_cart_id(model.getUser_cart_id());
        response.setItem_id(model.getItem_id());
        return response;
    }

}

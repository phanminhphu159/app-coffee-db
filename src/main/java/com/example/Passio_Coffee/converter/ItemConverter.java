package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.Item;
import com.example.Passio_Coffee.model.client.Request.ItemRequest;
import com.example.Passio_Coffee.model.db.Entity.ItemEntity;

@Component
public class ItemConverter implements ModelConverter<ItemRequest, ItemEntity, Item> {

    @Override
    public ItemEntity requestToModel(ItemRequest request) {
        ItemEntity model = new ItemEntity();
        model.setItem_id(request.getItem_id());
        model.setShop_id(request.getShop_id());
        model.setCategory_item_id(request.getCategory_item_id());
        model.setPromotion_id(request.getPromotion_id());
        model.setName(request.getName());
        model.setImage(request.getImage());
        model.setPrice(request.getPrice());
        model.setDescription(request.getDescription());
        model.setLikes(request.getLikes());
        model.setAvg_rating(request.getAvg_rating());
        model.setItem_reviews_id(request.getItem_reviews_id());
        return model;
    }

    @Override
    public Item modelToResponse(ItemEntity model) {
        Item response = new Item();
        response.setItem_id(model.getItem_id());
        response.setShop_id(model.getShop_id());
        response.setCategory_item_id(model.getCategory_item_id());
        response.setPromotion_id(model.getPromotion_id());
        response.setName(model.getName());
        response.setImage(model.getImage());
        response.setPrice(model.getPrice());
        response.setDescription(model.getDescription());
        response.setLikes(model.getLikes());
        response.setAvg_rating(model.getAvg_rating());
        response.setItem_reviews_id(model.getItem_reviews_id());
        return response;
    }

}

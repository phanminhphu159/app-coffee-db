package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.ShopReviews;
import com.example.Passio_Coffee.model.client.Request.ShopReviewsRequest;
import com.example.Passio_Coffee.model.db.Entity.ShopReviewsEntity;

@Component
public class ShopReviewsConverter implements ModelConverter<ShopReviewsRequest, ShopReviewsEntity, ShopReviews> {

    @Override
    public ShopReviewsEntity requestToModel(ShopReviewsRequest request) {
        ShopReviewsEntity model = new ShopReviewsEntity();
        model.setShop_reviews_id(request.getShop_reviews_id());
        model.setReview_id(request.getReview_id());
        return model;
    }

    @Override
    public ShopReviews modelToResponse(ShopReviewsEntity model) {
        ShopReviews response = new ShopReviews();
        response.setShop_reviews_id(model.getShop_reviews_id());
        response.setReview_id(model.getReview_id());
        return response;
    }

}

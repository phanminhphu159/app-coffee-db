package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.Notification;
import com.example.Passio_Coffee.model.client.Request.NotificationRequest;
import com.example.Passio_Coffee.model.db.Entity.NotificationEntity;

@Component
public class NotificationConverter implements ModelConverter<NotificationRequest, NotificationEntity, Notification> {

    @Override
    public NotificationEntity requestToModel(NotificationRequest request) {
        NotificationEntity model = new NotificationEntity();
        model.setNotification_id(request.getNotification_id());
        model.setContent(request.getContent());
        model.setImage(request.getImage());
        model.setCreated_at(request.getCreated_at());
        model.setStatus(request.getStatus());
        return model;
    }

    @Override
    public Notification modelToResponse(NotificationEntity model) {
        Notification response = new Notification();
        response.setNotification_id(model.getNotification_id());
        response.setContent(model.getContent());
        response.setImage(model.getImage());
        response.setCreated_at(model.getCreated_at());
        response.setStatus(model.getStatus());
        return response;
    }

}

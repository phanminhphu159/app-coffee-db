package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.UserCard;
import com.example.Passio_Coffee.model.client.Request.UserCardRequest;
import com.example.Passio_Coffee.model.db.Entity.UserCardEntity;

@Component
public class UserCardConverter implements ModelConverter<UserCardRequest, UserCardEntity, UserCard> {

    @Override
    public UserCardEntity requestToModel(UserCardRequest request) {
        UserCardEntity model = new UserCardEntity();
        model.setUser_card_id(request.getUser_card_id());
        model.setType(request.getType());
        model.setReward(request.getReward());
        model.setCard_coupons_id(request.getCard_coupons_id());
        return model;
    }

    @Override
    public UserCard modelToResponse(UserCardEntity model) {
        UserCard response = new UserCard();
        response.setUser_card_id(model.getUser_card_id());
        response.setType(model.getType());
        response.setReward(model.getReward());
        response.setCard_coupons_id(model.getCard_coupons_id());
        return response;
    }

}

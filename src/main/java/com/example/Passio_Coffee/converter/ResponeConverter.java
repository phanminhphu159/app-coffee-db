package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.Respone;
import com.example.Passio_Coffee.model.client.Request.ResponeRequest;
import com.example.Passio_Coffee.model.db.Entity.ResponeEntity;

@Component
public class ResponeConverter implements ModelConverter<ResponeRequest, ResponeEntity, Respone> {

    @Override
    public ResponeEntity requestToModel(ResponeRequest request) {
        ResponeEntity model = new ResponeEntity();
        model.setRespone_id(request.getRespone_id());
        model.setShop_id(request.getShop_id());
        model.setItem_id(request.getItem_id());
        model.setContent(request.getContent());
        model.setImage(request.getImage());
        model.setTime(request.getTime());
        return model;
    }

    @Override
    public Respone modelToResponse(ResponeEntity model) {
        Respone response = new Respone();
        response.setRespone_id(model.getRespone_id());
        response.setShop_id(model.getShop_id());
        response.setItem_id(model.getItem_id());
        response.setContent(model.getContent());
        response.setImage(model.getImage());
        response.setTime(model.getTime());
        return response;
    }

}

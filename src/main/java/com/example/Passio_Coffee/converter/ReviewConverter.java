package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.Review;
import com.example.Passio_Coffee.model.client.Request.ReviewRequest;
import com.example.Passio_Coffee.model.db.Entity.ReviewEntity;

@Component
public class ReviewConverter implements ModelConverter<ReviewRequest, ReviewEntity, Review> {

    @Override
    public ReviewEntity requestToModel(ReviewRequest request) {
        ReviewEntity model = new ReviewEntity();
        model.setReview_id(request.getReview_id());
        model.setUser_id(request.getUser_id());
        model.setContent(request.getContent());
        model.setImage(request.getImage());
        model.setRating(request.getRating());
        model.setTime(request.getTime());
        model.setRespone_id(request.getRespone_id());
        return model;
    }

    @Override
    public Review modelToResponse(ReviewEntity model) {
        Review response = new Review();
        response.setReview_id(model.getReview_id());
        response.setUser_id(model.getUser_id());
        response.setContent(model.getContent());
        response.setImage(model.getImage());
        response.setRating(model.getRating());
        response.setTime(model.getTime());
        response.setRespone_id(model.getRespone_id());
        return response;
    }

}

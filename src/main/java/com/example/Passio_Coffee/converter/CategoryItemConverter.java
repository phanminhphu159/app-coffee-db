package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.CategoryItem;
import com.example.Passio_Coffee.model.client.Request.CategoryItemRequest;
import com.example.Passio_Coffee.model.db.Entity.CategoryItemEntity;

@Component
public class CategoryItemConverter implements ModelConverter<CategoryItemRequest, CategoryItemEntity, CategoryItem> {

    @Override
    public CategoryItemEntity requestToModel(CategoryItemRequest request) {
        CategoryItemEntity model = new CategoryItemEntity();
        model.setCategory_id(request.getCategory_id());
        model.setItem_id(request.getItem_id());
        return model;
    }

    @Override
    public CategoryItem modelToResponse(CategoryItemEntity model) {
        CategoryItem response = new CategoryItem();
        response.setCategory_id(model.getCategory_id());
        response.setItem_id(model.getItem_id());
        return response;
    }

}

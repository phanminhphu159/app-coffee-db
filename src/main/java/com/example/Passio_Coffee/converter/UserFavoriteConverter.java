package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.UserFavorite;
import com.example.Passio_Coffee.model.client.Request.UserFavoriteRequest;
import com.example.Passio_Coffee.model.db.Entity.UserFavoriteEntity;

@Component
public class UserFavoriteConverter implements ModelConverter<UserFavoriteRequest, UserFavoriteEntity, UserFavorite> {

    @Override
    public UserFavoriteEntity requestToModel(UserFavoriteRequest request) {
        UserFavoriteEntity model = new UserFavoriteEntity();
        model.setUser_favorite_id(request.getUser_favorite_id());
        model.setItem_id(request.getItem_id());
        return model;
    }

    @Override
    public UserFavorite modelToResponse(UserFavoriteEntity model) {
        UserFavorite response = new UserFavorite();
        response.setUser_favorite_id(model.getUser_favorite_id());
        response.setItem_id(model.getItem_id());
        return response;
    }

}

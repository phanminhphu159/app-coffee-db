package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.User;
import com.example.Passio_Coffee.model.client.Request.UserRequest;
import com.example.Passio_Coffee.model.db.Entity.UserEntity;

@Component
public class UserConverter implements ModelConverter<UserRequest, UserEntity, User> {

    @Override
    public UserEntity requestToModel(UserRequest request) {
        UserEntity model = new UserEntity();
        model.setName(request.getName());
        model.setAvatar(request.getAvatar());
        model.setPhone(request.getPhone());
        model.setAddress(request.getAddress());
        model.setUser_order_id(request.getUser_order_id());
        model.setUser_card_id(request.getUser_card_id());
        model.setUser_favorite_id(request.getUser_favorite_id());
        model.setUser_notification_id(request.getUser_notification_id());
        model.setUser_cart_id(request.getUser_cart_id());
        return model;
    }

    @Override
    public User modelToResponse(UserEntity model) {
        User response = new User();
        response.setUser_id(model.getUser_id());
        response.setName(model.getName());
        response.setAvatar(model.getAvatar());
        response.setPhone(model.getPhone());
        response.setAddress(model.getAddress());
        response.setUser_order_id(model.getUser_order_id());
        response.setUser_card_id(model.getUser_card_id());
        response.setUser_favorite_id(model.getUser_favorite_id());
        response.setUser_notification_id(model.getUser_notification_id());
        response.setUser_cart_id(model.getUser_cart_id());
        return response;
    }

}

package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.Shop;
import com.example.Passio_Coffee.model.client.Request.ShopRequest;
import com.example.Passio_Coffee.model.db.Entity.ShopEntity;

@Component
public class ShopConverter implements ModelConverter<ShopRequest, ShopEntity, Shop> {

    @Override
    public ShopEntity requestToModel(ShopRequest request) {
        ShopEntity model = new ShopEntity();
        model.setShop_id(request.getShop_id());
        model.setName(request.getName());
        model.setImage(request.getImage());
        model.setAddress(request.getAddress());
        model.setPhone(request.getPhone());
        model.setLikes(request.getLikes());
        model.setAvg_rating(request.getAvg_rating());
        model.setShop_reviews_id(request.getShop_reviews_id());
        model.setShop_list_item(request.getShop_list_item());
        return model;
    }

    @Override
    public Shop modelToResponse(ShopEntity model) {
        Shop response = new Shop();
        response.setShop_id(model.getShop_id());
        response.setName(model.getName());
        response.setImage(model.getImage());
        response.setAddress(model.getAddress());
        response.setPhone(model.getPhone());
        response.setLikes(model.getLikes());
        response.setAvg_rating(model.getAvg_rating());
        response.setShop_reviews_id(model.getShop_reviews_id());
        response.setShop_list_item(model.getShop_list_item());
        return response;
    }

}

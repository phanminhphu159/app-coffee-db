package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.Coupon;
import com.example.Passio_Coffee.model.client.Request.CouponRequest;
import com.example.Passio_Coffee.model.db.Entity.CouponEntity;

@Component
public class CouponConverter implements ModelConverter<CouponRequest, CouponEntity, Coupon> {

    @Override
    public CouponEntity requestToModel(CouponRequest request) {
        CouponEntity model = new CouponEntity();
        model.setCoupon_id(request.getCoupon_id());
        model.setFrom_time(request.getFrom_time());
        model.setTo_time(request.getTo_time());
        model.setType_of_coupon(request.getType_of_coupon());
        model.setType_of_card(request.getType_of_card());
        model.setPrice_condition(request.getPrice_condition());
        model.setDiscount(request.getDiscount());
        model.setStatus(request.getStatus());
        return model;
    }

    @Override
    public Coupon modelToResponse(CouponEntity model) {
        Coupon response = new Coupon();
        response.setCoupon_id(model.getCoupon_id());
        response.setFrom_time(model.getFrom_time());
        response.setTo_time(model.getTo_time());
        response.setType_of_coupon(model.getType_of_coupon());
        response.setType_of_card(model.getType_of_card());
        response.setPrice_condition(model.getPrice_condition());
        response.setDiscount(model.getDiscount());
        response.setStatus(model.getStatus());
        return response;
    }

}

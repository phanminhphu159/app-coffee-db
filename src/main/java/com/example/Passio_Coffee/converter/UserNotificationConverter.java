package com.example.Passio_Coffee.converter;

import org.springframework.stereotype.Component;

import com.example.Passio_Coffee.converter.common.ModelConverter;
import com.example.Passio_Coffee.model.client.Respone.UserNotification;
import com.example.Passio_Coffee.model.client.Request.UserNotificationRequest;
import com.example.Passio_Coffee.model.db.Entity.UserNotificationEntity;

@Component
public class UserNotificationConverter implements ModelConverter<UserNotificationRequest, UserNotificationEntity, UserNotification> {

    @Override
    public UserNotificationEntity requestToModel(UserNotificationRequest request) {
        UserNotificationEntity model = new UserNotificationEntity();
        model.setUser_notification_id(request.getUser_notification_id());
        model.setNotification_id(request.getNotification_id());
        return model;
    }

    @Override
    public UserNotification modelToResponse(UserNotificationEntity model) {
        UserNotification response = new UserNotification();
        response.setUser_notification_id(model.getUser_notification_id());
        response.setNotification_id(model.getNotification_id());
        return response;
    }

}

package com.example.Passio_Coffee.model.client.Respone;

public class ItemReviews {
	private Long item_reviews_id;
    private Long review_id;
	
	public ItemReviews() {
    }

	public Long getItem_reviews_id() {
		return item_reviews_id;
	}

	public void setItem_reviews_id(Long item_reviews_id) {
		this.item_reviews_id = item_reviews_id;
	}

	public Long getReview_id() {
		return review_id;
	}

	public void setReview_id(Long review_id) {
		this.review_id = review_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item_reviews_id == null) ? 0 : item_reviews_id.hashCode());
		result = prime * result + ((review_id == null) ? 0 : review_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemReviews other = (ItemReviews) obj;
		if (item_reviews_id == null) {
			if (other.item_reviews_id != null)
				return false;
		} else if (!item_reviews_id.equals(other.item_reviews_id))
			return false;
		if (review_id == null) {
			if (other.review_id != null)
				return false;
		} else if (!review_id.equals(other.review_id))
			return false;
		return true;
	}
	
}

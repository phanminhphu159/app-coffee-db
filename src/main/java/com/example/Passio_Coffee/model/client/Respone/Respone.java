package com.example.Passio_Coffee.model.client.Respone;

import java.util.Arrays;
import java.util.Date;

public class Respone {
	private Long respone_id;
    private Long shop_id;
    private Long item_id;
    private String content;
    private byte[] image;
    private Date time;

    
    public Respone() {
    }


	public Long getRespone_id() {
		return respone_id;
	}


	public void setRespone_id(Long respone_id) {
		this.respone_id = respone_id;
	}


	public Long getShop_id() {
		return shop_id;
	}


	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}


	public Long getItem_id() {
		return item_id;
	}


	public void setItem_id(Long item_id) {
		this.item_id = item_id;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public byte[] getImage() {
		return image;
	}


	public void setImage(byte[] image) {
		this.image = image;
	}


	public Date getTime() {
		return time;
	}


	public void setTime(Date time) {
		this.time = time;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + Arrays.hashCode(image);
		result = prime * result + ((item_id == null) ? 0 : item_id.hashCode());
		result = prime * result + ((respone_id == null) ? 0 : respone_id.hashCode());
		result = prime * result + ((shop_id == null) ? 0 : shop_id.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Respone other = (Respone) obj;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (!Arrays.equals(image, other.image))
			return false;
		if (item_id == null) {
			if (other.item_id != null)
				return false;
		} else if (!item_id.equals(other.item_id))
			return false;
		if (respone_id == null) {
			if (other.respone_id != null)
				return false;
		} else if (!respone_id.equals(other.respone_id))
			return false;
		if (shop_id == null) {
			if (other.shop_id != null)
				return false;
		} else if (!shop_id.equals(other.shop_id))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}
    
    
}

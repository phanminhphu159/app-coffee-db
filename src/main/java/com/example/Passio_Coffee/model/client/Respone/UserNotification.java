package com.example.Passio_Coffee.model.client.Respone;

public class UserNotification {
	private Long user_notification_id;
    private Long notification_id;
	
	public UserNotification() {
    }

	public Long getUser_notification_id() {
		return user_notification_id;
	}

	public void setUser_notification_id(Long user_notification_id) {
		this.user_notification_id = user_notification_id;
	}

	public Long getNotification_id() {
		return notification_id;
	}

	public void setNotification_id(Long notification_id) {
		this.notification_id = notification_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((notification_id == null) ? 0 : notification_id.hashCode());
		result = prime * result + ((user_notification_id == null) ? 0 : user_notification_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserNotification other = (UserNotification) obj;
		if (notification_id == null) {
			if (other.notification_id != null)
				return false;
		} else if (!notification_id.equals(other.notification_id))
			return false;
		if (user_notification_id == null) {
			if (other.user_notification_id != null)
				return false;
		} else if (!user_notification_id.equals(other.user_notification_id))
			return false;
		return true;
	}
	
	
}

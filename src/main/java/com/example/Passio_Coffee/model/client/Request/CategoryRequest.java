package com.example.Passio_Coffee.model.client.Request;

import java.util.Arrays;

public class CategoryRequest {
	private Long category_id;
    private String category_item_name;
    private byte[] image;
	
	public CategoryRequest	() {
	
	}

	public Long getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Long category_id) {
		this.category_id = category_id;
	}

	public String getCategory_item_name() {
		return category_item_name;
	}

	public void setCategory_item_name(String category_item_name) {
		this.category_item_name = category_item_name;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category_id == null) ? 0 : category_id.hashCode());
		result = prime * result + ((category_item_name == null) ? 0 : category_item_name.hashCode());
		result = prime * result + Arrays.hashCode(image);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CategoryRequest other = (CategoryRequest) obj;
		if (category_id == null) {
			if (other.category_id != null)
				return false;
		} else if (!category_id.equals(other.category_id))
			return false;
		if (category_item_name == null) {
			if (other.category_item_name != null)
				return false;
		} else if (!category_item_name.equals(other.category_item_name))
			return false;
		if (!Arrays.equals(image, other.image))
			return false;
		return true;
	}
	
	
}

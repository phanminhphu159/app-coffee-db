package com.example.Passio_Coffee.model.client.Request;

import java.util.Arrays;

public class UserRequest {

    private String name;
    private byte[] avatar;
    private String phone;
    private String address;
    private Long user_order_id;
    private Long user_card_id;
    private Long user_favorite_id;
    private Long user_notification_id;
    private Long user_cart_id;

    public UserRequest() {
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getAvatar() {
		return avatar;
	}

	public void setAvatar(byte[] avatar) {
		this.avatar = avatar;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getUser_order_id() {
		return user_order_id;
	}

	public void setUser_order_id(Long user_order_id) {
		this.user_order_id = user_order_id;
	}

	public Long getUser_card_id() {
		return user_card_id;
	}

	public void setUser_card_id(Long user_card_id) {
		this.user_card_id = user_card_id;
	}

	public Long getUser_favorite_id() {
		return user_favorite_id;
	}

	public void setUser_favorite_id(Long user_favorite_id) {
		this.user_favorite_id = user_favorite_id;
	}

	public Long getUser_notification_id() {
		return user_notification_id;
	}

	public void setUser_notification_id(Long user_notification_id) {
		this.user_notification_id = user_notification_id;
	}

	public Long getUser_cart_id() {
		return user_cart_id;
	}

	public void setUser_cart_id(Long user_cart_id) {
		this.user_cart_id = user_cart_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + Arrays.hashCode(avatar);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((user_card_id == null) ? 0 : user_card_id.hashCode());
		result = prime * result + ((user_cart_id == null) ? 0 : user_cart_id.hashCode());
		result = prime * result + ((user_favorite_id == null) ? 0 : user_favorite_id.hashCode());
		result = prime * result + ((user_notification_id == null) ? 0 : user_notification_id.hashCode());
		result = prime * result + ((user_order_id == null) ? 0 : user_order_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRequest other = (UserRequest) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (!Arrays.equals(avatar, other.avatar))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (user_card_id == null) {
			if (other.user_card_id != null)
				return false;
		} else if (!user_card_id.equals(other.user_card_id))
			return false;
		if (user_cart_id == null) {
			if (other.user_cart_id != null)
				return false;
		} else if (!user_cart_id.equals(other.user_cart_id))
			return false;
		if (user_favorite_id == null) {
			if (other.user_favorite_id != null)
				return false;
		} else if (!user_favorite_id.equals(other.user_favorite_id))
			return false;
		if (user_notification_id == null) {
			if (other.user_notification_id != null)
				return false;
		} else if (!user_notification_id.equals(other.user_notification_id))
			return false;
		if (user_order_id == null) {
			if (other.user_order_id != null)
				return false;
		} else if (!user_order_id.equals(other.user_order_id))
			return false;
		return true;
	}

	
    	
}

package com.example.Passio_Coffee.model.client.Request;


public class UserCartRequest {
	
	private Long user_cart_id;
    private Long item_id;
	
	public UserCartRequest() {
    }

	public Long getUser_cart_id() {
		return user_cart_id;
	}

	public void setUser_cart_id(Long user_cart_id) {
		this.user_cart_id = user_cart_id;
	}

	public Long getItem_id() {
		return item_id;
	}

	public void setItem_id(Long item_id) {
		this.item_id = item_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item_id == null) ? 0 : item_id.hashCode());
		result = prime * result + ((user_cart_id == null) ? 0 : user_cart_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserCartRequest other = (UserCartRequest) obj;
		if (item_id == null) {
			if (other.item_id != null)
				return false;
		} else if (!item_id.equals(other.item_id))
			return false;
		if (user_cart_id == null) {
			if (other.user_cart_id != null)
				return false;
		} else if (!user_cart_id.equals(other.user_cart_id))
			return false;
		return true;
	}
}

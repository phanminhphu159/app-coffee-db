package com.example.Passio_Coffee.model.client.Request;


public class PromotionItemRequest {
	
	private Long promotion_id;
    private Long item_id;
	
	public PromotionItemRequest() {
    }

	public Long getPromotion_id() {
		return promotion_id;
	}

	public void setPromotion_id(Long promotion_id) {
		this.promotion_id = promotion_id;
	}

	public Long getItem_id() {
		return item_id;
	}

	public void setItem_id(Long item_id) {
		this.item_id = item_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item_id == null) ? 0 : item_id.hashCode());
		result = prime * result + ((promotion_id == null) ? 0 : promotion_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PromotionItemRequest other = (PromotionItemRequest) obj;
		if (item_id == null) {
			if (other.item_id != null)
				return false;
		} else if (!item_id.equals(other.item_id))
			return false;
		if (promotion_id == null) {
			if (other.promotion_id != null)
				return false;
		} else if (!promotion_id.equals(other.promotion_id))
			return false;
		return true;
	}
	
	
}

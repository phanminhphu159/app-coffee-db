package com.example.Passio_Coffee.model.client.Respone;

import java.util.Date;

public class Coupon {
	private Long coupon_id;
    private Date from_time;
    private Date to_time;
    private String type_of_coupon;
    private String type_of_card;
    private String price_condition;
    private String discount;
    private String status;
    
    
    public Coupon() {
    }


	public Long getCoupon_id() {
		return coupon_id;
	}


	public void setCoupon_id(Long coupon_id) {
		this.coupon_id = coupon_id;
	}


	public Date getFrom_time() {
		return from_time;
	}


	public void setFrom_time(Date from_time) {
		this.from_time = from_time;
	}


	public Date getTo_time() {
		return to_time;
	}


	public void setTo_time(Date to_time) {
		this.to_time = to_time;
	}


	public String getType_of_coupon() {
		return type_of_coupon;
	}


	public void setType_of_coupon(String type_of_coupon) {
		this.type_of_coupon = type_of_coupon;
	}


	public String getType_of_card() {
		return type_of_card;
	}


	public void setType_of_card(String type_of_card) {
		this.type_of_card = type_of_card;
	}


	public String getPrice_condition() {
		return price_condition;
	}


	public void setPrice_condition(String price_condition) {
		this.price_condition = price_condition;
	}


	public String getDiscount() {
		return discount;
	}


	public void setDiscount(String discount) {
		this.discount = discount;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coupon_id == null) ? 0 : coupon_id.hashCode());
		result = prime * result + ((discount == null) ? 0 : discount.hashCode());
		result = prime * result + ((from_time == null) ? 0 : from_time.hashCode());
		result = prime * result + ((price_condition == null) ? 0 : price_condition.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((to_time == null) ? 0 : to_time.hashCode());
		result = prime * result + ((type_of_card == null) ? 0 : type_of_card.hashCode());
		result = prime * result + ((type_of_coupon == null) ? 0 : type_of_coupon.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coupon other = (Coupon) obj;
		if (coupon_id == null) {
			if (other.coupon_id != null)
				return false;
		} else if (!coupon_id.equals(other.coupon_id))
			return false;
		if (discount == null) {
			if (other.discount != null)
				return false;
		} else if (!discount.equals(other.discount))
			return false;
		if (from_time == null) {
			if (other.from_time != null)
				return false;
		} else if (!from_time.equals(other.from_time))
			return false;
		if (price_condition == null) {
			if (other.price_condition != null)
				return false;
		} else if (!price_condition.equals(other.price_condition))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (to_time == null) {
			if (other.to_time != null)
				return false;
		} else if (!to_time.equals(other.to_time))
			return false;
		if (type_of_card == null) {
			if (other.type_of_card != null)
				return false;
		} else if (!type_of_card.equals(other.type_of_card))
			return false;
		if (type_of_coupon == null) {
			if (other.type_of_coupon != null)
				return false;
		} else if (!type_of_coupon.equals(other.type_of_coupon))
			return false;
		return true;
	}
    
}

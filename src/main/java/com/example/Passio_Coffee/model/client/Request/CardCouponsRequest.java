package com.example.Passio_Coffee.model.client.Request;


public class CardCouponsRequest {
	 private Long user_card_id;
	 private Long coupon_id;
	 
	 public CardCouponsRequest() {
	 }

	public Long getUser_card_id() {
		return user_card_id;
	}

	public void setUser_card_id(Long user_card_id) {
		this.user_card_id = user_card_id;
	}

	public Long getCoupon_id() {
		return coupon_id;
	}

	public void setCoupon_id(Long coupon_id) {
		this.coupon_id = coupon_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coupon_id == null) ? 0 : coupon_id.hashCode());
		result = prime * result + ((user_card_id == null) ? 0 : user_card_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CardCouponsRequest other = (CardCouponsRequest) obj;
		if (coupon_id == null) {
			if (other.coupon_id != null)
				return false;
		} else if (!coupon_id.equals(other.coupon_id))
			return false;
		if (user_card_id == null) {
			if (other.user_card_id != null)
				return false;
		} else if (!user_card_id.equals(other.user_card_id))
			return false;
		return true;
	}
	 
	 
}

package com.example.Passio_Coffee.model.client.Request;

import java.util.Arrays;

public class ItemRequest {
	private Long item_id;
    private Long shop_id;
    private Long category_item_id;
    private Long promotion_id;
    private String name;
    private byte[] image;
    private Double price;
    private String description;
    private String likes;
    private Double avg_rating;
    private Long item_reviews_id;
	
    public ItemRequest() {
    }

	public Long getItem_id() {
		return item_id;
	}

	public void setItem_id(Long item_id) {
		this.item_id = item_id;
	}

	public Long getShop_id() {
		return shop_id;
	}

	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}

	public Long getCategory_item_id() {
		return category_item_id;
	}

	public void setCategory_item_id(Long category_item_id) {
		this.category_item_id = category_item_id;
	}

	public Long getPromotion_id() {
		return promotion_id;
	}

	public void setPromotion_id(Long promotion_id) {
		this.promotion_id = promotion_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLikes() {
		return likes;
	}

	public void setLikes(String likes) {
		this.likes = likes;
	}

	public Double getAvg_rating() {
		return avg_rating;
	}

	public void setAvg_rating(Double avg_rating) {
		this.avg_rating = avg_rating;
	}

	public Long getItem_reviews_id() {
		return item_reviews_id;
	}

	public void setItem_reviews_id(Long item_reviews_id) {
		this.item_reviews_id = item_reviews_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((avg_rating == null) ? 0 : avg_rating.hashCode());
		result = prime * result + ((category_item_id == null) ? 0 : category_item_id.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + Arrays.hashCode(image);
		result = prime * result + ((item_id == null) ? 0 : item_id.hashCode());
		result = prime * result + ((item_reviews_id == null) ? 0 : item_reviews_id.hashCode());
		result = prime * result + ((likes == null) ? 0 : likes.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((promotion_id == null) ? 0 : promotion_id.hashCode());
		result = prime * result + ((shop_id == null) ? 0 : shop_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemRequest other = (ItemRequest) obj;
		if (avg_rating == null) {
			if (other.avg_rating != null)
				return false;
		} else if (!avg_rating.equals(other.avg_rating))
			return false;
		if (category_item_id == null) {
			if (other.category_item_id != null)
				return false;
		} else if (!category_item_id.equals(other.category_item_id))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (!Arrays.equals(image, other.image))
			return false;
		if (item_id == null) {
			if (other.item_id != null)
				return false;
		} else if (!item_id.equals(other.item_id))
			return false;
		if (item_reviews_id == null) {
			if (other.item_reviews_id != null)
				return false;
		} else if (!item_reviews_id.equals(other.item_reviews_id))
			return false;
		if (likes == null) {
			if (other.likes != null)
				return false;
		} else if (!likes.equals(other.likes))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (promotion_id == null) {
			if (other.promotion_id != null)
				return false;
		} else if (!promotion_id.equals(other.promotion_id))
			return false;
		if (shop_id == null) {
			if (other.shop_id != null)
				return false;
		} else if (!shop_id.equals(other.shop_id))
			return false;
		return true;
	}
    
    
}

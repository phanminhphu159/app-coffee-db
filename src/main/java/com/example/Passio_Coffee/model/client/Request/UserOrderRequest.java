package com.example.Passio_Coffee.model.client.Request;

import java.util.Date;

public class UserOrderRequest {
	
	private Long user_order_id;
    private Long shop_id;
    private Long ordered_item_list_id;
    private Date from_time;
    private Date to_time;
    private String status;
    
    public UserOrderRequest() {
    }

	public Long getUser_order_id() {
		return user_order_id;
	}

	public void setUser_order_id(Long user_order_id) {
		this.user_order_id = user_order_id;
	}

	public Long getShop_id() {
		return shop_id;
	}

	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}

	public Long getOrdered_item_list_id() {
		return ordered_item_list_id;
	}

	public void setOrdered_item_list_id(Long ordered_item_list_id) {
		this.ordered_item_list_id = ordered_item_list_id;
	}

	public Date getFrom_time() {
		return from_time;
	}

	public void setFrom_time(Date from_time) {
		this.from_time = from_time;
	}

	public Date getTo_time() {
		return to_time;
	}

	public void setTo_time(Date to_time) {
		this.to_time = to_time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((from_time == null) ? 0 : from_time.hashCode());
		result = prime * result + ((ordered_item_list_id == null) ? 0 : ordered_item_list_id.hashCode());
		result = prime * result + ((shop_id == null) ? 0 : shop_id.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((to_time == null) ? 0 : to_time.hashCode());
		result = prime * result + ((user_order_id == null) ? 0 : user_order_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserOrderRequest other = (UserOrderRequest) obj;
		if (from_time == null) {
			if (other.from_time != null)
				return false;
		} else if (!from_time.equals(other.from_time))
			return false;
		if (ordered_item_list_id == null) {
			if (other.ordered_item_list_id != null)
				return false;
		} else if (!ordered_item_list_id.equals(other.ordered_item_list_id))
			return false;
		if (shop_id == null) {
			if (other.shop_id != null)
				return false;
		} else if (!shop_id.equals(other.shop_id))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (to_time == null) {
			if (other.to_time != null)
				return false;
		} else if (!to_time.equals(other.to_time))
			return false;
		if (user_order_id == null) {
			if (other.user_order_id != null)
				return false;
		} else if (!user_order_id.equals(other.user_order_id))
			return false;
		return true;
	}
    
    
}

package com.example.Passio_Coffee.model.client.Respone;

import java.util.Date;

public class Promotion {
	private Long promotion_id;
    private String name;
    private Date from_time;
    private Date to_time;
	
	public Promotion() {
    }

	public Long getPromotion_id() {
		return promotion_id;
	}

	public void setPromotion_id(Long promotion_id) {
		this.promotion_id = promotion_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getFrom_time() {
		return from_time;
	}

	public void setFrom_time(Date from_time) {
		this.from_time = from_time;
	}

	public Date getTo_time() {
		return to_time;
	}

	public void setTo_time(Date to_time) {
		this.to_time = to_time;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((from_time == null) ? 0 : from_time.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((promotion_id == null) ? 0 : promotion_id.hashCode());
		result = prime * result + ((to_time == null) ? 0 : to_time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Promotion other = (Promotion) obj;
		if (from_time == null) {
			if (other.from_time != null)
				return false;
		} else if (!from_time.equals(other.from_time))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (promotion_id == null) {
			if (other.promotion_id != null)
				return false;
		} else if (!promotion_id.equals(other.promotion_id))
			return false;
		if (to_time == null) {
			if (other.to_time != null)
				return false;
		} else if (!to_time.equals(other.to_time))
			return false;
		return true;
	}
	
	
}

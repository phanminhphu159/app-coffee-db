package com.example.Passio_Coffee.model.db.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SHOP_REVIEWS")
public class ShopReviewsEntity {
	
	@Id
	@Column
    private Long shop_reviews_id;

	@Column
    private Long review_id;
	
	public ShopReviewsEntity() {
    }

	public Long getShop_reviews_id() {
		return shop_reviews_id;
	}

	public void setShop_reviews_id(Long shop_reviews_id) {
		this.shop_reviews_id = shop_reviews_id;
	}

	public Long getReview_id() {
		return review_id;
	}

	public void setReview_id(Long review_id) {
		this.review_id = review_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((review_id == null) ? 0 : review_id.hashCode());
		result = prime * result + ((shop_reviews_id == null) ? 0 : shop_reviews_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShopReviewsEntity other = (ShopReviewsEntity) obj;
		if (review_id == null) {
			if (other.review_id != null)
				return false;
		} else if (!review_id.equals(other.review_id))
			return false;
		if (shop_reviews_id == null) {
			if (other.shop_reviews_id != null)
				return false;
		} else if (!shop_reviews_id.equals(other.shop_reviews_id))
			return false;
		return true;
	}
	
	
}

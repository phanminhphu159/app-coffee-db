package com.example.Passio_Coffee.model.db.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="ITEM")
public class ItemEntity {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
    private Long item_id;

	@Column
    private Long shop_id;

	@Column
    private Long category_item_id;

	@Column
    private Long promotion_id;
	
    @Column
    private String name;
	
	@Lob
    @Column
    private byte[] image;

    @Column
    private Double price;

    @Column
    private String description;

    @Column
    private String likes;
    
    @Column
    private Double avg_rating;
    
	@Column
    private Long item_reviews_id;
	
	@OneToMany(mappedBy = "item_id", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PromotionItemEntity> PromotionItems = new ArrayList<>();


    public ItemEntity() {
    }

	
	public Long getItem_id() {
		return item_id;
	}

	public void setItem_id(Long item_id) {
		this.item_id = item_id;
	}

	public Long getShop_id() {
		return shop_id;
	}

	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}

	public Long getCategory_item_id() {
		return category_item_id;
	}

	public void setCategory_item_id(Long category_item_id) {
		this.category_item_id = category_item_id;
	}

	public Long getPromotion_id() {
		return promotion_id;
	}

	public void setPromotion_id(Long promotion_id) {
		this.promotion_id = promotion_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLikes() {
		return likes;
	}

	public void setLikes(String likes) {
		this.likes = likes;
	}

	public Double getAvg_rating() {
		return avg_rating;
	}

	public void setAvg_rating(Double avg_rating) {
		this.avg_rating = avg_rating;
	}

	public Long getItem_reviews_id() {
		return item_reviews_id;
	}

	public void setItem_reviews_id(Long item_reviews_id) {
		this.item_reviews_id = item_reviews_id;
	}
	
	
	
	
    
}

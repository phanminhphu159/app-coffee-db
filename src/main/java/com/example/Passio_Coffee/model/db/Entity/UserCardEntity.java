package com.example.Passio_Coffee.model.db.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USER_CARD")
public class UserCardEntity {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
    private Long user_card_id;
    
    @Column
    private String type;

    @Column
    private String reward;
    
	@Column
    private Long card_coupons_id;
	
	public UserCardEntity() {
    }


	public Long getUser_card_id() {
		return user_card_id;
	}

	public void setUser_card_id(Long user_card_id) {
		this.user_card_id = user_card_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReward() {
		return reward;
	}

	public void setReward(String reward) {
		this.reward = reward;
	}

	public Long getCard_coupons_id() {
		return card_coupons_id;
	}

	public void setCard_coupons_id(Long card_coupons_id) {
		this.card_coupons_id = card_coupons_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((card_coupons_id == null) ? 0 : card_coupons_id.hashCode());
		result = prime * result + ((reward == null) ? 0 : reward.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((user_card_id == null) ? 0 : user_card_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserCardEntity other = (UserCardEntity) obj;
		if (card_coupons_id == null) {
			if (other.card_coupons_id != null)
				return false;
		} else if (!card_coupons_id.equals(other.card_coupons_id))
			return false;
		if (reward == null) {
			if (other.reward != null)
				return false;
		} else if (!reward.equals(other.reward))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (user_card_id == null) {
			if (other.user_card_id != null)
				return false;
		} else if (!user_card_id.equals(other.user_card_id))
			return false;
		return true;
	}
    
}

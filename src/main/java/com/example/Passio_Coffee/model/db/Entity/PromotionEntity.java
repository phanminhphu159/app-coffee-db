package com.example.Passio_Coffee.model.db.Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="PROMOTION")
public class PromotionEntity {
	
	@Id
	@Column
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long promotion_id ;
	
	@Column
    private String name;
    
	@Column
    private Date from_time;
	
	@Column
    private Date to_time;
	
	@OneToMany(mappedBy = "promotion_id", cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "promotion_id", referencedColumnName = "promotion_id")
    private List<PromotionEntity> promotionEntitys = new ArrayList<>();


    public PromotionEntity() {
    }
	
	public Long getPromotion_id() {
		return promotion_id;
	}

	public void setPromotion_id(Long promotion_id) {
		this.promotion_id = promotion_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getFrom_time() {
		return from_time;
	}

	public void setFrom_time(Date from_time) {
		this.from_time = from_time;
	}

	public Date getTo_time() {
		return to_time;
	}

	public void setTo_time(Date to_time) {
		this.to_time = to_time;
	}
	
	
}

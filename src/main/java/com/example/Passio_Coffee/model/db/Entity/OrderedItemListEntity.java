package com.example.Passio_Coffee.model.db.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ORDERED_ITEM_LIST")
public class OrderedItemListEntity {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
    private Long ordered_item_list_id;

	@Column
    private Long item_id;
	

	public OrderedItemListEntity() {
    }


	public Long getOrdered_item_list_id() {
		return ordered_item_list_id;
	}


	public void setOrdered_item_list_id(Long ordered_item_list_id) {
		this.ordered_item_list_id = ordered_item_list_id;
	}


	public Long getItem_id() {
		return item_id;
	}


	public void setItem_id(Long item_id) {
		this.item_id = item_id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item_id == null) ? 0 : item_id.hashCode());
		result = prime * result + ((ordered_item_list_id == null) ? 0 : ordered_item_list_id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderedItemListEntity other = (OrderedItemListEntity) obj;
		if (item_id == null) {
			if (other.item_id != null)
				return false;
		} else if (!item_id.equals(other.item_id))
			return false;
		if (ordered_item_list_id == null) {
			if (other.ordered_item_list_id != null)
				return false;
		} else if (!ordered_item_list_id.equals(other.ordered_item_list_id))
			return false;
		return true;
	}
}

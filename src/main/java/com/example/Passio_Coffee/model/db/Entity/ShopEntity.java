package com.example.Passio_Coffee.model.db.Entity;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="SHOP")
public class ShopEntity {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
    private Long shop_id;

	@Column
    private String name;
	
	@Lob
    @Column
    private byte[] image;

    @Column
    private String address;
    
    @Column
    private String phone;
    
    @Column
    private String likes;
    
    @Column
    private Double avg_rating;

    @Column
    private Long shop_reviews_id;

    @Column
    private Long shop_list_item;
    
    public ShopEntity() {
    }

	public Long getShop_id() {
		return shop_id;
	}

	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLikes() {
		return likes;
	}

	public void setLikes(String likes) {
		this.likes = likes;
	}

	public Double getAvg_rating() {
		return avg_rating;
	}

	public void setAvg_rating(Double avg_rating) {
		this.avg_rating = avg_rating;
	}

	public Long getShop_reviews_id() {
		return shop_reviews_id;
	}

	public void setShop_reviews_id(Long shop_reviews_id) {
		this.shop_reviews_id = shop_reviews_id;
	}

	public Long getShop_list_item() {
		return shop_list_item;
	}

	public void setShop_list_item(Long shop_list_item) {
		this.shop_list_item = shop_list_item;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((avg_rating == null) ? 0 : avg_rating.hashCode());
		result = prime * result + Arrays.hashCode(image);
		result = prime * result + ((likes == null) ? 0 : likes.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((shop_id == null) ? 0 : shop_id.hashCode());
		result = prime * result + ((shop_list_item == null) ? 0 : shop_list_item.hashCode());
		result = prime * result + ((shop_reviews_id == null) ? 0 : shop_reviews_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShopEntity other = (ShopEntity) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (avg_rating == null) {
			if (other.avg_rating != null)
				return false;
		} else if (!avg_rating.equals(other.avg_rating))
			return false;
		if (!Arrays.equals(image, other.image))
			return false;
		if (likes == null) {
			if (other.likes != null)
				return false;
		} else if (!likes.equals(other.likes))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (shop_id == null) {
			if (other.shop_id != null)
				return false;
		} else if (!shop_id.equals(other.shop_id))
			return false;
		if (shop_list_item == null) {
			if (other.shop_list_item != null)
				return false;
		} else if (!shop_list_item.equals(other.shop_list_item))
			return false;
		if (shop_reviews_id == null) {
			if (other.shop_reviews_id != null)
				return false;
		} else if (!shop_reviews_id.equals(other.shop_reviews_id))
			return false;
		return true;
	}

    
}

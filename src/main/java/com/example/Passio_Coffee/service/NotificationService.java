package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.NotificationConverter;
import com.example.Passio_Coffee.model.client.Respone.Notification;
import com.example.Passio_Coffee.model.client.Request.NotificationRequest;
import com.example.Passio_Coffee.model.db.Entity.NotificationEntity;
import com.example.Passio_Coffee.repository.NotificationRepository;

@Service
@Transactional
public class NotificationService {

    @Autowired
    NotificationConverter NotificationConverter;

    @Autowired
    NotificationRepository NotificationRepository;
    
    public Collection<Notification> getAll() {
        return NotificationConverter.modelToResponse(NotificationRepository.findAll());
    }

    public Notification save(final NotificationRequest NotificationRequest) {
        NotificationEntity NotificationEntity = NotificationConverter.requestToModel(NotificationRequest);

        // NotificationEntity.setAddTs(new Date());

        return NotificationConverter.modelToResponse(NotificationRepository.save(NotificationEntity));
    }
    
    public Notification update(final Long id, final NotificationRequest NotificationRequest) {
        NotificationEntity fromRequest = NotificationConverter.requestToModel(NotificationRequest);

        NotificationEntity toSave = NotificationRepository.getOne(id);
        toSave.setNotification_id(fromRequest.getNotification_id());
        toSave.setContent(fromRequest.getContent());
        toSave.setImage(fromRequest.getImage());
        toSave.setCreated_at(fromRequest.getCreated_at());
        toSave.setStatus(fromRequest.getStatus());

        return NotificationConverter.modelToResponse(NotificationRepository.save(toSave));
    }

    public void delete(final Long id) {
        NotificationRepository.deleteById(id);
    }
    
}


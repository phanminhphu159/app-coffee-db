package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.UserConverter;
import com.example.Passio_Coffee.model.client.Respone.User;
import com.example.Passio_Coffee.model.client.Request.UserRequest;
import com.example.Passio_Coffee.model.db.Entity.UserEntity;
import com.example.Passio_Coffee.repository.UserRepository;

@Service
@Transactional
public class UserService {

    @Autowired
    UserConverter userConverter;

    @Autowired
    UserRepository userRepository;
    
    public Collection<User> getAll() {
        return userConverter.modelToResponse(userRepository.findAll());
    }

    public User save(final UserRequest userRequest) {
        UserEntity UserEntity = userConverter.requestToModel(userRequest);

        // UserEntity.setAddTs(new Date());

        return userConverter.modelToResponse(userRepository.save(UserEntity));
    }
    
    public User update(final Long id, final UserRequest userRequest) {
        UserEntity fromRequest = userConverter.requestToModel(userRequest);

        UserEntity toSave = userRepository.getOne(id);
        toSave.setName(fromRequest.getName());
        toSave.setAvatar(fromRequest.getAvatar());
        toSave.setPhone(fromRequest.getPhone());
        toSave.setAddress(fromRequest.getAddress());
        toSave.setUser_order_id(fromRequest.getUser_order_id());
        toSave.setUser_card_id(fromRequest.getUser_card_id());
        toSave.setUser_favorite_id(fromRequest.getUser_favorite_id());
        toSave.setUser_notification_id(fromRequest.getUser_notification_id());
        toSave.setUser_cart_id(fromRequest.getUser_cart_id());

        return userConverter.modelToResponse(userRepository.save(toSave));
    }

    public void delete(final Long id) {
        userRepository.deleteById(id);
    }
    
}


package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.OrderedItemListConverter;
import com.example.Passio_Coffee.model.client.Respone.OrderedItemList;
import com.example.Passio_Coffee.model.client.Request.OrderedItemListRequest;
import com.example.Passio_Coffee.model.db.Entity.OrderedItemListEntity;
import com.example.Passio_Coffee.repository.OrderedItemListRepository;

@Service
@Transactional
public class OrderedItemListService {

    @Autowired
    OrderedItemListConverter OrderedItemListConverter;

    @Autowired
    OrderedItemListRepository OrderedItemListRepository;
    
    public Collection<OrderedItemList> getAll() {
        return OrderedItemListConverter.modelToResponse(OrderedItemListRepository.findAll());
    }

    public OrderedItemList save(final OrderedItemListRequest OrderedItemListRequest) {
        OrderedItemListEntity OrderedItemListEntity = OrderedItemListConverter.requestToModel(OrderedItemListRequest);

        // OrderedItemListEntity.setAddTs(new Date());

        return OrderedItemListConverter.modelToResponse(OrderedItemListRepository.save(OrderedItemListEntity));
    }
    
    public OrderedItemList update(final Long id, final OrderedItemListRequest OrderedItemListRequest) {
        OrderedItemListEntity fromRequest = OrderedItemListConverter.requestToModel(OrderedItemListRequest);

        OrderedItemListEntity toSave = OrderedItemListRepository.getOne(id);
        toSave.setOrdered_item_list_id(fromRequest.getOrdered_item_list_id());
        toSave.setItem_id(fromRequest.getItem_id());

        return OrderedItemListConverter.modelToResponse(OrderedItemListRepository.save(toSave));
    }

    public void delete(final Long id) {
        OrderedItemListRepository.deleteById(id);
    }
    
}


package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.CouponConverter;
import com.example.Passio_Coffee.model.client.Respone.Coupon;
import com.example.Passio_Coffee.model.client.Request.CouponRequest;
import com.example.Passio_Coffee.model.db.Entity.CouponEntity;
import com.example.Passio_Coffee.repository.CouponRepository;

@Service
@Transactional
public class CouponService {

    @Autowired
    CouponConverter CouponConverter;

    @Autowired
    CouponRepository CouponRepository;
    
    public Collection<Coupon> getAll() {
        return CouponConverter.modelToResponse(CouponRepository.findAll());
    }

    public Coupon save(final CouponRequest CouponRequest) {
        CouponEntity CouponEntity = CouponConverter.requestToModel(CouponRequest);

        // CouponEntity.setAddTs(new Date());

        return CouponConverter.modelToResponse(CouponRepository.save(CouponEntity));
    }
    
    public Coupon update(final Long id, final CouponRequest CouponRequest) {
        CouponEntity fromRequest = CouponConverter.requestToModel(CouponRequest);

        CouponEntity toSave = CouponRepository.getOne(id);
        toSave.setCoupon_id(fromRequest.getCoupon_id());
        toSave.setFrom_time(fromRequest.getFrom_time());
        toSave.setTo_time(fromRequest.getTo_time());
        toSave.setType_of_coupon(fromRequest.getType_of_coupon());
        toSave.setType_of_card(fromRequest.getType_of_card());
        toSave.setPrice_condition(fromRequest.getPrice_condition());
        toSave.setDiscount(fromRequest.getDiscount());
        toSave.setStatus(fromRequest.getStatus());

        return CouponConverter.modelToResponse(CouponRepository.save(toSave));
    }

    public void delete(final Long id) {
        CouponRepository.deleteById(id);
    }
    
}


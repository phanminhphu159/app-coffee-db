package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.CategoryConverter;
import com.example.Passio_Coffee.model.client.Respone.Category;
import com.example.Passio_Coffee.model.client.Request.CategoryRequest;
import com.example.Passio_Coffee.model.db.Entity.CategoryEntity;
import com.example.Passio_Coffee.repository.CategoryRepository;

@Service
@Transactional
public class CategoryService {

    @Autowired
    CategoryConverter CategoryConverter;

    @Autowired
    CategoryRepository CategoryRepository;
    
    public Collection<Category> getAll() {
        return CategoryConverter.modelToResponse(CategoryRepository.findAll());
    }

    public Category save(final CategoryRequest CategoryRequest) {
        CategoryEntity CategoryEntity = CategoryConverter.requestToModel(CategoryRequest);

        // CategoryEntity.setAddTs(new Date());

        return CategoryConverter.modelToResponse(CategoryRepository.save(CategoryEntity));
    }
    
    public Category update(final Long id, final CategoryRequest CategoryRequest) {
        CategoryEntity fromRequest = CategoryConverter.requestToModel(CategoryRequest);

        CategoryEntity toSave = CategoryRepository.getOne(id);
        toSave.setCategory_id(fromRequest.getCategory_id());
        toSave.setCategory_item_name(fromRequest.getCategory_item_name());
        toSave.setImage(fromRequest.getImage());

        return CategoryConverter.modelToResponse(CategoryRepository.save(toSave));
    }

    public void delete(final Long id) {
        CategoryRepository.deleteById(id);
    }
    
}


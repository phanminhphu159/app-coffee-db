package com.example.Passio_Coffee.service;

import java.util.Collection;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.PromotionItemConverter;
import com.example.Passio_Coffee.model.client.Respone.PromotionItem;
import com.example.Passio_Coffee.model.client.Request.PromotionItemRequest;
import com.example.Passio_Coffee.model.db.Entity.ItemEntity;
import com.example.Passio_Coffee.model.db.Entity.PromotionEntity;
import com.example.Passio_Coffee.model.db.Entity.PromotionItemEntity;
import com.example.Passio_Coffee.repository.PromotionItemRepository;

@Service
@Transactional
public class PromotionItemService {

    @Autowired
    PromotionItemConverter PromotionItemConverter;

    @Autowired
    PromotionItemRepository PromotionItemRepository;
    
    public Collection<PromotionItem> getAll() {
        return PromotionItemConverter.modelToResponse(PromotionItemRepository.findAll());
    }

    public PromotionItem save(final PromotionItemRequest PromotionItemRequest) {
        PromotionItemEntity PromotionItemEntity = PromotionItemConverter.requestToModel(PromotionItemRequest);

        // PromotionItemEntity.setAddTs(new Date());

        return PromotionItemConverter.modelToResponse(PromotionItemRepository.save(PromotionItemEntity));
    }
    
    public PromotionItem update(final Long id, final PromotionItemRequest PromotionItemRequest) {
        PromotionItemEntity fromRequest = PromotionItemConverter.requestToModel(PromotionItemRequest);

        PromotionItemEntity toSave = PromotionItemRepository.getOne(id);
        toSave.setPromotion_id(fromRequest.getPromotion_id());
        toSave.setItem_id(fromRequest.getItem_id());

        return PromotionItemConverter.modelToResponse(PromotionItemRepository.save(toSave));
    }

    public void delete(final Long id) {
        PromotionItemRepository.deleteById(id);
    }
    
}

package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.UserCartConverter;
import com.example.Passio_Coffee.model.client.Respone.UserCart;
import com.example.Passio_Coffee.model.client.Request.UserCartRequest;
import com.example.Passio_Coffee.model.db.Entity.UserCartEntity;
import com.example.Passio_Coffee.repository.UserCartRepository;

@Service
@Transactional
public class UserCartService {

    @Autowired
    UserCartConverter UserCartConverter;

    @Autowired
    UserCartRepository UserCartRepository;
    
    public Collection<UserCart> getAll() {
        return UserCartConverter.modelToResponse(UserCartRepository.findAll());
    }

    public UserCart save(final UserCartRequest UserCartRequest) {
        UserCartEntity UserCartEntity = UserCartConverter.requestToModel(UserCartRequest);

        // UserCartEntity.setAddTs(new Date());

        return UserCartConverter.modelToResponse(UserCartRepository.save(UserCartEntity));
    }
    
    public UserCart update(final Long id, final UserCartRequest UserCartRequest) {
        UserCartEntity fromRequest = UserCartConverter.requestToModel(UserCartRequest);

        UserCartEntity toSave = UserCartRepository.getOne(id);
        toSave.setUser_cart_id(fromRequest.getUser_cart_id());
        toSave.setItem_id(fromRequest.getItem_id());

        return UserCartConverter.modelToResponse(UserCartRepository.save(toSave));
    }

    public void delete(final Long id) {
        UserCartRepository.deleteById(id);
    }
    
}


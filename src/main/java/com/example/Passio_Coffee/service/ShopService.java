package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.ShopConverter;
import com.example.Passio_Coffee.model.client.Respone.Shop;
import com.example.Passio_Coffee.model.client.Request.ShopRequest;
import com.example.Passio_Coffee.model.db.Entity.ShopEntity;
import com.example.Passio_Coffee.repository.ShopRepository;

@Service
@Transactional
public class ShopService {

    @Autowired
    ShopConverter ShopConverter;

    @Autowired
    ShopRepository ShopRepository;
    
    public Collection<Shop> getAll() {
        return ShopConverter.modelToResponse(ShopRepository.findAll());
    }

    public Shop save(final ShopRequest ShopRequest) {
        ShopEntity ShopEntity = ShopConverter.requestToModel(ShopRequest);

        // ShopEntity.setAddTs(new Date());

        return ShopConverter.modelToResponse(ShopRepository.save(ShopEntity));
    }
    
    public Shop update(final Long id, final ShopRequest ShopRequest) {
        ShopEntity fromRequest = ShopConverter.requestToModel(ShopRequest);

        ShopEntity toSave = ShopRepository.getOne(id);
        toSave.setShop_id(fromRequest.getShop_id());
        toSave.setName(fromRequest.getName());
        toSave.setImage(fromRequest.getImage());
        toSave.setAddress(fromRequest.getAddress());
        toSave.setPhone(fromRequest.getPhone());
        toSave.setLikes(fromRequest.getLikes());
        toSave.setAvg_rating(fromRequest.getAvg_rating());
        toSave.setShop_reviews_id(fromRequest.getShop_reviews_id());
        toSave.setShop_list_item(fromRequest.getShop_list_item());

        return ShopConverter.modelToResponse(ShopRepository.save(toSave));
    }

    public void delete(final Long id) {
        ShopRepository.deleteById(id);
    }
    
}


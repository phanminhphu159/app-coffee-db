package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.ItemConverter;
import com.example.Passio_Coffee.model.client.Respone.Item;
import com.example.Passio_Coffee.model.client.Request.ItemRequest;
import com.example.Passio_Coffee.model.db.Entity.ItemEntity;
import com.example.Passio_Coffee.repository.ItemRepository;

@Service
@Transactional
public class ItemService {

    @Autowired
    ItemConverter ItemConverter;

    @Autowired
    ItemRepository ItemRepository;
    
    public Collection<Item> getAll() {
        return ItemConverter.modelToResponse(ItemRepository.findAll());
    }

    public Item save(final ItemRequest ItemRequest) {
        ItemEntity ItemEntity = ItemConverter.requestToModel(ItemRequest);

        // ItemEntity.setAddTs(new Date());

        return ItemConverter.modelToResponse(ItemRepository.save(ItemEntity));
    }
    
    public Item update(final Long id, final ItemRequest ItemRequest) {
        ItemEntity fromRequest = ItemConverter.requestToModel(ItemRequest);

        ItemEntity toSave = ItemRepository.getOne(id);
        toSave.setItem_id(fromRequest.getItem_id());
        toSave.setShop_id(fromRequest.getShop_id());
        toSave.setCategory_item_id(fromRequest.getCategory_item_id());
        toSave.setPromotion_id(fromRequest.getPromotion_id());
        toSave.setName(fromRequest.getName());
        toSave.setImage(fromRequest.getImage());
        toSave.setPrice(fromRequest.getPrice());
        toSave.setDescription(fromRequest.getDescription());
        toSave.setLikes(fromRequest.getLikes());
        toSave.setAvg_rating(fromRequest.getAvg_rating());
        toSave.setItem_reviews_id(fromRequest.getItem_reviews_id());

        return ItemConverter.modelToResponse(ItemRepository.save(toSave));
    }

    public void delete(final Long id) {
        ItemRepository.deleteById(id);
    }
    
}


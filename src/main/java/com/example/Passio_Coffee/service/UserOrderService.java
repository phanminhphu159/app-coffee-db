package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.UserOrderConverter;
import com.example.Passio_Coffee.model.client.Respone.UserOrder;
import com.example.Passio_Coffee.model.client.Request.UserOrderRequest;
import com.example.Passio_Coffee.model.db.Entity.UserOrderEntity;
import com.example.Passio_Coffee.repository.UserOrderRepository;

@Service
@Transactional
public class UserOrderService {

    @Autowired
    UserOrderConverter UserOrderConverter;

    @Autowired
    UserOrderRepository UserOrderRepository;
    
    public Collection<UserOrder> getAll() {
        return UserOrderConverter.modelToResponse(UserOrderRepository.findAll());
    }

    public UserOrder save(final UserOrderRequest UserOrderRequest) {
        UserOrderEntity UserOrderEntity = UserOrderConverter.requestToModel(UserOrderRequest);

        // UserOrderEntity.setAddTs(new Date());

        return UserOrderConverter.modelToResponse(UserOrderRepository.save(UserOrderEntity));
    }
    
    public UserOrder update(final Long id, final UserOrderRequest UserOrderRequest) {
        UserOrderEntity fromRequest = UserOrderConverter.requestToModel(UserOrderRequest);

        UserOrderEntity toSave = UserOrderRepository.getOne(id);
        toSave.setUser_order_id(fromRequest.getUser_order_id());
        toSave.setShop_id(fromRequest.getShop_id());
        toSave.setOrdered_item_list_id(fromRequest.getOrdered_item_list_id());
        toSave.setFrom_time(fromRequest.getFrom_time());
        toSave.setTo_time(fromRequest.getTo_time());
        toSave.setStatus(fromRequest.getStatus());

        return UserOrderConverter.modelToResponse(UserOrderRepository.save(toSave));
    }

    public void delete(final Long id) {
        UserOrderRepository.deleteById(id);
    }
    
}


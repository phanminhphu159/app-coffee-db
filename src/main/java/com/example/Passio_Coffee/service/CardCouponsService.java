package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.CardCouponsConverter;
import com.example.Passio_Coffee.model.client.Respone.CardCoupons;
import com.example.Passio_Coffee.model.client.Request.CardCouponsRequest;
import com.example.Passio_Coffee.model.db.Entity.CardCouponsEntity;
import com.example.Passio_Coffee.repository.CardCouponsRepository;

@Service
@Transactional
public class CardCouponsService {

    @Autowired
    CardCouponsConverter CardCouponsConverter;

    @Autowired
    CardCouponsRepository CardCouponsRepository;
    
    public Collection<CardCoupons> getAll() {
        return CardCouponsConverter.modelToResponse(CardCouponsRepository.findAll());
    }

    public CardCoupons save(final CardCouponsRequest CardCouponsRequest) {
        CardCouponsEntity CardCouponsEntity = CardCouponsConverter.requestToModel(CardCouponsRequest);

        // CardCouponsEntity.setAddTs(new Date());

        return CardCouponsConverter.modelToResponse(CardCouponsRepository.save(CardCouponsEntity));
    }
    
    public CardCoupons update(final Long id, final CardCouponsRequest CardCouponsRequest) {
        CardCouponsEntity fromRequest = CardCouponsConverter.requestToModel(CardCouponsRequest);

        CardCouponsEntity toSave = CardCouponsRepository.getOne(id);
        toSave.setUser_card_id(fromRequest.getUser_card_id());
        toSave.setCoupon_id(fromRequest.getCoupon_id());
        return CardCouponsConverter.modelToResponse(CardCouponsRepository.save(toSave));
    }

    public void delete(final Long id) {
        CardCouponsRepository.deleteById(id);
    }
    
}


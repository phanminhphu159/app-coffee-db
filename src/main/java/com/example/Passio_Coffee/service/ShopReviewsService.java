package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.ShopReviewsConverter;
import com.example.Passio_Coffee.model.client.Respone.ShopReviews;
import com.example.Passio_Coffee.model.client.Request.ShopReviewsRequest;
import com.example.Passio_Coffee.model.db.Entity.ShopReviewsEntity;
import com.example.Passio_Coffee.repository.ShopReviewsRepository;

@Service
@Transactional
public class ShopReviewsService {

    @Autowired
    ShopReviewsConverter ShopReviewsConverter;

    @Autowired
    ShopReviewsRepository ShopReviewsRepository;
    
    public Collection<ShopReviews> getAll() {
        return ShopReviewsConverter.modelToResponse(ShopReviewsRepository.findAll());
    }

    public ShopReviews save(final ShopReviewsRequest ShopReviewsRequest) {
        ShopReviewsEntity ShopReviewsEntity = ShopReviewsConverter.requestToModel(ShopReviewsRequest);

        // ShopReviewsEntity.setAddTs(new Date());

        return ShopReviewsConverter.modelToResponse(ShopReviewsRepository.save(ShopReviewsEntity));
    }
    
    public ShopReviews update(final Long id, final ShopReviewsRequest ShopReviewsRequest) {
        ShopReviewsEntity fromRequest = ShopReviewsConverter.requestToModel(ShopReviewsRequest);

        ShopReviewsEntity toSave = ShopReviewsRepository.getOne(id);
        toSave.setShop_reviews_id(fromRequest.getShop_reviews_id());
        toSave.setReview_id(fromRequest.getReview_id());

        return ShopReviewsConverter.modelToResponse(ShopReviewsRepository.save(toSave));
    }

    public void delete(final Long id) {
        ShopReviewsRepository.deleteById(id);
    }
    
}


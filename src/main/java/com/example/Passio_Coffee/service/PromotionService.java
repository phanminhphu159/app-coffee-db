package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.PromotionConverter;
import com.example.Passio_Coffee.model.client.Respone.Promotion;
import com.example.Passio_Coffee.model.client.Request.PromotionRequest;
import com.example.Passio_Coffee.model.db.Entity.PromotionEntity;
import com.example.Passio_Coffee.repository.PromotionRepository;

@Service
@Transactional
public class PromotionService {

    @Autowired
    PromotionConverter PromotionConverter;

    @Autowired
    PromotionRepository PromotionRepository;
    
    public Collection<Promotion> getAll() {
        return PromotionConverter.modelToResponse(PromotionRepository.findAll());
    }

    public Promotion save(final PromotionRequest PromotionRequest) {
        PromotionEntity PromotionEntity = PromotionConverter.requestToModel(PromotionRequest);

        // PromotionEntity.setAddTs(new Date());

        return PromotionConverter.modelToResponse(PromotionRepository.save(PromotionEntity));
    }
    
    public Promotion update(final Long id, final PromotionRequest PromotionRequest) {
        PromotionEntity fromRequest = PromotionConverter.requestToModel(PromotionRequest);

        PromotionEntity toSave = PromotionRepository.getOne(id);
        toSave.setPromotion_id(fromRequest.getPromotion_id());
        toSave.setName(fromRequest.getName());
        toSave.setFrom_time(fromRequest.getFrom_time());
        toSave.setTo_time(fromRequest.getTo_time());

        return PromotionConverter.modelToResponse(PromotionRepository.save(toSave));
    }

    public void delete(final Long id) {
        PromotionRepository.deleteById(id);
    }
    
}


package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.ReviewConverter;
import com.example.Passio_Coffee.model.client.Respone.Review;
import com.example.Passio_Coffee.model.client.Request.ReviewRequest;
import com.example.Passio_Coffee.model.db.Entity.ReviewEntity;
import com.example.Passio_Coffee.repository.ReviewRepository;

@Service
@Transactional
public class ReviewService {

    @Autowired
    ReviewConverter ReviewConverter;

    @Autowired
    ReviewRepository ReviewRepository;
    
    public Collection<Review> getAll() {
        return ReviewConverter.modelToResponse(ReviewRepository.findAll());
    }

    public Review save(final ReviewRequest ReviewRequest) {
        ReviewEntity ReviewEntity = ReviewConverter.requestToModel(ReviewRequest);

        // ReviewEntity.setAddTs(new Date());

        return ReviewConverter.modelToResponse(ReviewRepository.save(ReviewEntity));
    }
    
    public Review update(final Long id, final ReviewRequest ReviewRequest) {
        ReviewEntity fromRequest = ReviewConverter.requestToModel(ReviewRequest);

        ReviewEntity toSave = ReviewRepository.getOne(id);
        toSave.setReview_id(fromRequest.getReview_id());
        toSave.setUser_id(fromRequest.getUser_id());
        toSave.setContent(fromRequest.getContent());
        toSave.setImage(fromRequest.getImage());
        toSave.setRating(fromRequest.getRating());
        toSave.setTime(fromRequest.getTime());
        toSave.setRespone_id(fromRequest.getRespone_id());

        return ReviewConverter.modelToResponse(ReviewRepository.save(toSave));
    }

    public void delete(final Long id) {
        ReviewRepository.deleteById(id);
    }
    
}


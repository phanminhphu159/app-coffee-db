package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.UserNotificationConverter;
import com.example.Passio_Coffee.model.client.Respone.UserNotification;
import com.example.Passio_Coffee.model.client.Request.UserNotificationRequest;
import com.example.Passio_Coffee.model.db.Entity.UserNotificationEntity;
import com.example.Passio_Coffee.repository.UserNotificationRepository;

@Service
@Transactional
public class UserNotificationService {

    @Autowired
    UserNotificationConverter UserNotificationConverter;

    @Autowired
    UserNotificationRepository UserNotificationRepository;
    
    public Collection<UserNotification> getAll() {
        return UserNotificationConverter.modelToResponse(UserNotificationRepository.findAll());
    }

    public UserNotification save(final UserNotificationRequest UserNotificationRequest) {
        UserNotificationEntity UserNotificationEntity = UserNotificationConverter.requestToModel(UserNotificationRequest);

        // UserNotificationEntity.setAddTs(new Date());

        return UserNotificationConverter.modelToResponse(UserNotificationRepository.save(UserNotificationEntity));
    }
    
    public UserNotification update(final Long id, final UserNotificationRequest UserNotificationRequest) {
        UserNotificationEntity fromRequest = UserNotificationConverter.requestToModel(UserNotificationRequest);

        UserNotificationEntity toSave = UserNotificationRepository.getOne(id);
        toSave.setUser_notification_id(fromRequest.getUser_notification_id());
        toSave.setNotification_id(fromRequest.getNotification_id());

        return UserNotificationConverter.modelToResponse(UserNotificationRepository.save(toSave));
    }

    public void delete(final Long id) {
        UserNotificationRepository.deleteById(id);
    }
    
}


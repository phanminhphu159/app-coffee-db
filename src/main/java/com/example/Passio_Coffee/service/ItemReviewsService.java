package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.ItemReviewsConverter;
import com.example.Passio_Coffee.model.client.Respone.ItemReviews;
import com.example.Passio_Coffee.model.client.Request.ItemReviewsRequest;
import com.example.Passio_Coffee.model.db.Entity.ItemReviewsEntity;
import com.example.Passio_Coffee.repository.ItemReviewsRepository;

@Service
@Transactional
public class ItemReviewsService {

    @Autowired
    ItemReviewsConverter ItemReviewsConverter;

    @Autowired
    ItemReviewsRepository ItemReviewsRepository;
    
    public Collection<ItemReviews> getAll() {
        return ItemReviewsConverter.modelToResponse(ItemReviewsRepository.findAll());
    }

    public ItemReviews save(final ItemReviewsRequest ItemReviewsRequest) {
        ItemReviewsEntity ItemReviewsEntity = ItemReviewsConverter.requestToModel(ItemReviewsRequest);

        // ItemReviewsEntity.setAddTs(new Date());

        return ItemReviewsConverter.modelToResponse(ItemReviewsRepository.save(ItemReviewsEntity));
    }
    
    public ItemReviews update(final Long id, final ItemReviewsRequest ItemReviewsRequest) {
        ItemReviewsEntity fromRequest = ItemReviewsConverter.requestToModel(ItemReviewsRequest);

        ItemReviewsEntity toSave = ItemReviewsRepository.getOne(id);
        toSave.setItem_reviews_id(fromRequest.getItem_reviews_id());
        toSave.setReview_id(fromRequest.getReview_id());

        return ItemReviewsConverter.modelToResponse(ItemReviewsRepository.save(toSave));
    }

    public void delete(final Long id) {
        ItemReviewsRepository.deleteById(id);
    }
    
}


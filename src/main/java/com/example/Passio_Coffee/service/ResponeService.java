package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.ResponeConverter;
import com.example.Passio_Coffee.model.client.Respone.Respone;
import com.example.Passio_Coffee.model.client.Request.ResponeRequest;
import com.example.Passio_Coffee.model.db.Entity.ResponeEntity;
import com.example.Passio_Coffee.repository.ResponeRepository;

@Service
@Transactional
public class ResponeService {

    @Autowired
    ResponeConverter ResponeConverter;

    @Autowired
    ResponeRepository ResponeRepository;
    
    public Collection<Respone> getAll() {
        return ResponeConverter.modelToResponse(ResponeRepository.findAll());
    }

    public Respone save(final ResponeRequest ResponeRequest) {
        ResponeEntity ResponeEntity = ResponeConverter.requestToModel(ResponeRequest);

        // ResponeEntity.setAddTs(new Date());

        return ResponeConverter.modelToResponse(ResponeRepository.save(ResponeEntity));
    }
    
    public Respone update(final Long id, final ResponeRequest ResponeRequest) {
        ResponeEntity fromRequest = ResponeConverter.requestToModel(ResponeRequest);

        ResponeEntity toSave = ResponeRepository.getOne(id);
        toSave.setRespone_id(fromRequest.getRespone_id());
        toSave.setShop_id(fromRequest.getShop_id());
        toSave.setItem_id(fromRequest.getItem_id());
        toSave.setContent(fromRequest.getContent());
        toSave.setImage(fromRequest.getImage());
        toSave.setTime(fromRequest.getTime());

        return ResponeConverter.modelToResponse(ResponeRepository.save(toSave));
    }

    public void delete(final Long id) {
        ResponeRepository.deleteById(id);
    }
    
}


package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.UserFavoriteConverter;
import com.example.Passio_Coffee.model.client.Respone.UserFavorite;
import com.example.Passio_Coffee.model.client.Request.UserFavoriteRequest;
import com.example.Passio_Coffee.model.db.Entity.UserFavoriteEntity;
import com.example.Passio_Coffee.repository.UserFavoriteRepository;

@Service
@Transactional
public class UserFavoriteService {

    @Autowired
    UserFavoriteConverter UserFavoriteConverter;

    @Autowired
    UserFavoriteRepository UserFavoriteRepository;
    
    public Collection<UserFavorite> getAll() {
        return UserFavoriteConverter.modelToResponse(UserFavoriteRepository.findAll());
    }

    public UserFavorite save(final UserFavoriteRequest UserFavoriteRequest) {
        UserFavoriteEntity UserFavoriteEntity = UserFavoriteConverter.requestToModel(UserFavoriteRequest);

        // UserFavoriteEntity.setAddTs(new Date());

        return UserFavoriteConverter.modelToResponse(UserFavoriteRepository.save(UserFavoriteEntity));
    }
    
    public UserFavorite update(final Long id, final UserFavoriteRequest UserFavoriteRequest) {
        UserFavoriteEntity fromRequest = UserFavoriteConverter.requestToModel(UserFavoriteRequest);

        UserFavoriteEntity toSave = UserFavoriteRepository.getOne(id);
        toSave.setUser_favorite_id(fromRequest.getUser_favorite_id());
        toSave.setItem_id(fromRequest.getItem_id());

        return UserFavoriteConverter.modelToResponse(UserFavoriteRepository.save(toSave));
    }

    public void delete(final Long id) {
        UserFavoriteRepository.deleteById(id);
    }
    
}


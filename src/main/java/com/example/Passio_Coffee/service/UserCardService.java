package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.UserCardConverter;
import com.example.Passio_Coffee.model.client.Respone.UserCard;
import com.example.Passio_Coffee.model.client.Request.UserCardRequest;
import com.example.Passio_Coffee.model.db.Entity.UserCardEntity;
import com.example.Passio_Coffee.repository.UserCardRepository;

@Service
@Transactional
public class UserCardService {

    @Autowired
    UserCardConverter UserCardConverter;

    @Autowired
    UserCardRepository UserCardRepository;
    
    public Collection<UserCard> getAll() {
        return UserCardConverter.modelToResponse(UserCardRepository.findAll());
    }

    public UserCard save(final UserCardRequest UserCardRequest) {
        UserCardEntity UserCardEntity = UserCardConverter.requestToModel(UserCardRequest);

        // UserCardEntity.setAddTs(new Date());

        return UserCardConverter.modelToResponse(UserCardRepository.save(UserCardEntity));
    }
    
    public UserCard update(final Long id, final UserCardRequest UserCardRequest) {
        UserCardEntity fromRequest = UserCardConverter.requestToModel(UserCardRequest);

        UserCardEntity toSave = UserCardRepository.getOne(id);
        toSave.setUser_card_id(fromRequest.getUser_card_id());
        toSave.setType(fromRequest.getType());
        toSave.setReward(fromRequest.getReward());
        toSave.setCard_coupons_id(fromRequest.getCard_coupons_id());

        return UserCardConverter.modelToResponse(UserCardRepository.save(toSave));
    }

    public void delete(final Long id) {
        UserCardRepository.deleteById(id);
    }
    
}


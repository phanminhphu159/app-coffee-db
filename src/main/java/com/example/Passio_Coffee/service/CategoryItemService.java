package com.example.Passio_Coffee.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.Passio_Coffee.converter.CategoryItemConverter;
import com.example.Passio_Coffee.model.client.Respone.CategoryItem;
import com.example.Passio_Coffee.model.client.Request.CategoryItemRequest;
import com.example.Passio_Coffee.model.db.Entity.CategoryItemEntity;
import com.example.Passio_Coffee.repository.CategoryItemRepository;

@Service
@Transactional
public class CategoryItemService {

    @Autowired
    CategoryItemConverter CategoryItemConverter;

    @Autowired
    CategoryItemRepository CategoryItemRepository;
    
    public Collection<CategoryItem> getAll() {
        return CategoryItemConverter.modelToResponse(CategoryItemRepository.findAll());
    }

    public CategoryItem save(final CategoryItemRequest CategoryItemRequest) {
        CategoryItemEntity CategoryItemEntity = CategoryItemConverter.requestToModel(CategoryItemRequest);

        // CategoryItemEntity.setAddTs(new Date());

        return CategoryItemConverter.modelToResponse(CategoryItemRepository.save(CategoryItemEntity));
    }
    
    public CategoryItem update(final Long id, final CategoryItemRequest CategoryItemRequest) {
        CategoryItemEntity fromRequest = CategoryItemConverter.requestToModel(CategoryItemRequest);

        CategoryItemEntity toSave = CategoryItemRepository.getOne(id);
        toSave.setCategory_id(fromRequest.getCategory_id());
        toSave.setItem_id(fromRequest.getItem_id());

        return CategoryItemConverter.modelToResponse(CategoryItemRepository.save(toSave));
    }

    public void delete(final Long id) {
        CategoryItemRepository.deleteById(id);
    }
    
}

